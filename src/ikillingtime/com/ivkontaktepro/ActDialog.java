package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.v4.content.CursorLoader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import ikillingtime.com.ivkontaktepro.PopupAction.OnDismissListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import utils.imageloader.AsyncImageView;
import utils.imageloader.MaskImageProcessor;
import utils.serviceloader.RESTService;
import utils.serviceloader.RESTService2;
import widget.MyActionBar.AbstractAction;
import widget.ScrollingTextView;

import java.io.File;
import java.util.*;

public class ActDialog extends Activity {
	private static final String TAG = "ActDialog";
	private static final int CAPTCHA_RESULT = 0;
	private static final int VOICE_REQUEST  = 118;
	private ResultReceiver mReceiver;
	private AdpDialog adapter;
	private ArrayList<ClsDialog> dlgs;
	private CmpDialog mCmpDialog;
	private int dlgsSize;
	private String sig, longTs,longKey,longServer;
	private ListView list;
	private Activity activity;
	private EditText txt;
	private ClsDialog dlg;
	private String metod;
	private boolean paused = false,clearList = true,isRunning=false;;
	private int offset;
	public static  int ONDISMISSCODE = 0;
	private static final int TAKE_PHOTO = 20;
	private static final int CHOOSE_PHOTO = 21;
	private static final int CHOOSE_LOCATION = 22;
	private String mCurrentPhotoPath = "";
	private int currentUid;
	private AsyncImageView att1,att2,att3,att4,att5,att6;
	private ImageView del1,del2,del3,del4,del5,del6;
	private ArrayList<ClsAttPhoto> attachments;
	private HorizontalScrollView attll;
	InputMethodManager inputManager;
	private static final int SWIPE_MIN_DISTANCE = 140;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	private GestureDetector gestureDetector;
	private ScrollingTextView title;
	private TextView multichatcnt;
	private RelativeLayout topMenu;
	private LinearLayout subMenu;
	private Button voiceBtn;
	private ListView.OnTouchListener gestureListener;
	private ImageView progressBar;
	private ScrollingTextView status;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog);
        att1 = (AsyncImageView) findViewById(R.id.dlg_att_img1);
        att2 = (AsyncImageView) findViewById(R.id.dlg_att_img2);
        att3 = (AsyncImageView) findViewById(R.id.dlg_att_img3);
        att4 = (AsyncImageView) findViewById(R.id.dlg_att_img4);
        att5 = (AsyncImageView) findViewById(R.id.dlg_att_img5);
        att6 = (AsyncImageView) findViewById(R.id.dlg_att_img6);
        del1 = (ImageView) findViewById(R.id.dlg_del_img1);
        del2 = (ImageView) findViewById(R.id.dlg_del_img2);
        del3 = (ImageView) findViewById(R.id.dlg_del_img3);
        del4 = (ImageView) findViewById(R.id.dlg_del_img4);
        del5 = (ImageView) findViewById(R.id.dlg_del_img5);
        del6 = (ImageView) findViewById(R.id.dlg_del_img6);
        voiceBtn = (Button) findViewById(R.id.dlg_voicebtn);
        status = (ScrollingTextView)findViewById(R.id.dlg_status);
        status.setText("");
        progressBar = (ImageView) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        boolean hasVoice = true;
        PackageManager pm = getPackageManager();
        List<ResolveInfo> activities = pm.queryIntentActivities(
                new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        if (activities.size() == 0) {
        	voiceBtn.setVisibility(View.GONE);
        	hasVoice = false;
        }
        LinearLayout multichat = (LinearLayout) findViewById(R.id.dlg_multichat);
        topMenu = (RelativeLayout) findViewById(R.id.top_menu);
        subMenu = (LinearLayout) findViewById(R.id.sub_menu);
        subMenu.setVisibility(View.GONE);
        multichatcnt = (TextView) findViewById(R.id.dlg_multichatcnt);
        attll = (HorizontalScrollView) findViewById(R.id.dlg_attach_ll);
        title = (ScrollingTextView) findViewById(R.id.dlg_title);
        final ImageView imgOnline = (ImageView) findViewById(R.id.dlg_online);
        final AsyncImageView imgAvatar = (AsyncImageView) findViewById(R.id.dlg_useravatar);
        inputManager = (InputMethodManager) 
                getSystemService(INPUT_METHOD_SERVICE);

        Log.d(TAG, "init");
        activity = this;
        paused = false;

        dlg = AndroidApplication.getDlg();
        if (dlg==null) {dlg = new ClsDialog(); dlg.setUid(AndroidApplication.getPrefsInt("user_id"));}
        dlgs = new ArrayList<ClsDialog>();
        attachments = new ArrayList<ClsAttPhoto>();
        adapter = new AdpDialog(this,dlgs,dlg.getUid());
        mCmpDialog = new CmpDialog();
        dlgsSize = 0;
        offset = 0;
        longTs = "";
        longKey = "";
        longServer = "";
        metod = "/method/execute?access_token="+AndroidApplication.getPrefsString("access_token");
        
        txt = (EditText) findViewById(R.id.dlg_sndtxt);
        if (hasVoice) {
	        txt.addTextChangedListener(new TextWatcher(){
	    	    public void afterTextChanged(Editable s){
	    	    	if (s.toString().trim().equals("")) {
	    	    		voiceBtn.setVisibility(View.VISIBLE);
	    	    	}
	    	    	else 
	    	    		voiceBtn.setVisibility(View.GONE);
	    	    }
	    	    public void beforeTextChanged(CharSequence s, int start, int count, int after){	}
	    	    public void onTextChanged(CharSequence s, int start, int before, int count){ }
	    	});
        }
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnScrollListener(onScroll);
     // Gesture detection
	    gestureDetector = new GestureDetector(new MyGestureDetector());
	    gestureListener = new ListView.OnTouchListener() {
	        public boolean onTouch(View v, MotionEvent event) {
	            return gestureDetector.onTouchEvent(event);
	        }
	    };
        list.setOnTouchListener(gestureListener);
        list.setOnItemClickListener(onDlgClick);

        AnimationSet set = new AnimationSet(true);
        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(400);
        set.addAnimation(animation);
        animation = new TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
            Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(400);
        set.addAnimation(animation);

        LayoutAnimationController controller =
            new LayoutAnimationController(set, 0.25f);
        list.setLayoutAnimation(controller);

        
        ClsProfile p = dlg.getProfile();
        if (p==null) currentUid=-1; else currentUid = p.getUid();
        String tmp = p.getFirst_name()+ " "+ p.getLast_name();
    	if (tmp.equals("")) tmp = p.getScreen_name();
    	if (dlg.getChat_id()!=0) {
    		tmp = dlg.getTitle();
    		if (tmp.equals("")) getString(R.string.tab_con);
    		currentUid = -1;
    		imgOnline.setVisibility(View.GONE);
    		imgAvatar.setVisibility(View.GONE);
    		multichat.setVisibility(View.VISIBLE);
    		multichatcnt.setText(""+dlg.getUsers_count());
    	}
    	else {
    		multichat.setVisibility(View.GONE);
    		imgAvatar.setVisibility(View.VISIBLE);
    		if (p!=null) {
    			if (p.getOnline()==1) {
    				imgOnline.setVisibility(View.VISIBLE);
    			}
    			else imgOnline.setVisibility(View.GONE);
    			imgAvatar.setImageProcessor(new MaskImageProcessor(7));
    			imgAvatar.setUrl(p.getPhoto());
    		}
    	}
    	
    	title.setText(tmp);

        mReceiver = new ResultReceiver(new Handler()) {

            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey("REST_RESULT")) {
                    onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                }
                else {
                    onRESTResult(resultCode, null, 0);
                }
                
            }
            
        };
        String cashedSig = "";
        if (dlg.getChat_id()!=0) {
		    String code = "var messages = API.messages.getHistory({chat_id:"+dlg.getChat_id()+",count:100,offset:"+offset+"});" +
		    		"var profiles = API.getProfiles({uids: \""+dlg.getChat_active()+"\",fields: \"photo_medium,online,screen_name,contacts\"});" +
            		"return {messages: messages, profiles: profiles};";
		    cashedSig = Utils.md5(metod+"&code="+code+ AndroidApplication.getPrefsString("secret"));

        }
        else {
        	String code = "var messages = API.messages.getHistory({uid:"+dlg.getUid()+",count:100,offset:"+offset+"});" +
        		"return {messages: messages};";
        	cashedSig = Utils.md5(metod+"&code="+code+ AndroidApplication.getPrefsString("secret"));
        }
        //������ ���� �� ���
        String cacheResult = Utils.loadFromCashe(cashedSig);
        
		if (cacheResult!=null) {
			Log.d(TAG, "cashe");
			parseResult(cacheResult,true);
			Collections.sort(dlgs, mCmpDialog);
			clearList = false;
		}
		else {
			clearList = true;//TODO
		}
		onBrowse(-1);
    }

	
    private String restRequest(Bundle params,int httpVerb,int mode,String method) {
    	isRunning = true;
    	progressBar.setVisibility(View.VISIBLE);
    	progressBar.post(new Runnable() {
    	    @Override
    	    public void run() {
    	        AnimationDrawable frameAnimation =
    	            (AnimationDrawable) progressBar.getBackground();
    	        frameAnimation.start();
    	    }
    	});

    	StringBuilder paramsString = new StringBuilder();
    	SortedMap<String, String> paramsMap = new TreeMap<String, String>();
    	for (String key : params.keySet()) {
    		paramsMap.put(key, params.get(key).toString());
    	}

    	for (String key : paramsMap.keySet()) {
            paramsString.append("&");
            paramsString.append(key);
            paramsString.append("=");
            paramsString.append(paramsMap.get(key).toString());
    	}
    	if (method.equals("")) method = "execute";
    	String methodString = "/method/"+method+"?access_token="+AndroidApplication.getPrefsString("access_token");
		String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));

		Log.d(TAG, methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));
        Intent intent = new Intent(activity, RESTService.class);
        intent.setData(Uri.parse(AndroidApplication.HOST+methodString+"&sig="+sig));
        intent.putExtra(RESTService.EXTRA_MODE, mode);
        if (mode == 0x5) {
        	intent.putExtra(RESTService.EXTRA_FILE, mCurrentPhotoPath);
        	intent.putExtra(RESTService.EXTRA_HTTP_VERB, 0x5);
        }
        else {
        	intent.putExtra(RESTService.EXTRA_HTTP_VERB, httpVerb);
        }
        intent.putExtra(RESTService.EXTRA_PARAMS, params);
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
        activity.startService(intent);
        return sig;
	}
	
    public void onRESTResult(int code, String result, int mode) {
        // Here is where we handle our REST response. This is similar to the 
        // LoaderCallbacks<D>.onLoadFinished() call from the previous tutorial.
    	if (paused == true) return;
    	progressBar.setVisibility(View.GONE);
    	progressBar.clearAnimation();
        Log.d(TAG, "onRESTResult mode:"+mode+" res: "+result);
        // Check to see if we got an HTTP 200 code and have some data.
        if (code == 200 && result != null) {
        	//check captcha
        	if (result.contains("error_code\":14")) {
        		//parseCaptcha(result);
        		Intent intent = new Intent(this, ActCaptcha.class);
        		intent.putExtra("result", result);
		        startActivityForResult(intent, CAPTCHA_RESULT);
		        return;
        	}
        	//06-08 08:13:12.179: D/ActContacts(13228): onRESTResult:{"error":{"error_code":10,"error_msg":"Internal server error: Database problems, try later","request_params":[{"key":"oauth","value":"1"},{"key":"method","value":"execute"},{"key":"access_token","value":"ed333cbfee9d158fee9d158f7deeb02bbefee9dee94058da21afbf873d2728f"},{"key":"sig","value":"0f550287429d2b98fe911d803a80cdeb"},{"key":"code","value":"var profiles = API.friends.get({fields: \"photo_medium,online,screen_name,contacts\",order:\"hints\"});return {profiles: profiles};"}]}}
        	if (result.contains("error_msg")) {
 				try {
 					JSONObject o = new JSONObject(result);
 					o = o.optJSONObject("error");
 					Toast.makeText(activity, "Error:"+o.optString("error_msg"), Toast.LENGTH_LONG).show();
 				}
 				catch (Exception e) {}
 				return;
 			}
        	switch(mode) {
        	case -1:
        		//������� �� 0 - ��� ������
        		parseResult(result,false);//�������� ���
        		Collections.sort(dlgs, mCmpDialog);//������
        		isRunning = false;
        		//offset+=100;
        		break;
        	case 0:
        		//scroll mode
        		parseResult(result,false);//�������� ���
        		Collections.sort(dlgs, mCmpDialog);//������
        		isRunning = false;
        		offset+=100;
        		break;
        	case 1:
        		try {
					JSONObject mJSONObject = new JSONObject(result);
			    	mJSONObject = mJSONObject.optJSONObject("response");
			    	mJSONObject = mJSONObject.optJSONObject("longpoll");
			    	longKey = mJSONObject.optString("key");
			    	longServer = Uri.decode(mJSONObject.optString("server"));
			    	longTs = mJSONObject.optString("ts");
        		}
        		catch (Exception e) {}
        		if (!longKey.equals("") && !longServer.equals("") && !longTs.equals("")) {
        			getUpdates();
        			clearList = true;
        	        onBrowse(-1);
        		}
        		break;
        	case 2:
        		JSONArray updates = null; 
        		try{
        			JSONObject mJSONObject = new JSONObject(result);
			    	longTs = mJSONObject.optString("ts");
			    	updates = mJSONObject.optJSONArray("updates");
        		}
        		catch (Exception e) {}
        		if (!longTs.equals("")) {
        			if (updates == null || updates.length()==0) {
        				getUpdates();
        			}
        			else {
        				//parse updates
        				parseUpdates(updates);
        				Collections.sort(dlgs, mCmpDialog);
        			}
        		}
        		break;
        	case 5:
        		//onRESTResult:{"server":304913,"photo":"[]","hash":"a1ad9e16d65548ea5f1d8979912e270f"}
        		savePhoto(result);
        		break;
        	case 9:
        		//onRESTResult mode:9 res: {"response":[{"pid":286099476,"id":"photo97768291_286099476","aid":-3,"owner_id":97768291,"src":"http:\/\/cs316928.userapi.com\/v316928291\/1f4a\/uch2lQpFkmg.jpg","src_big":"http:\/\/cs316928.userapi.com\/v316928291\/1f4b\/GzWGeZJdr1A.jpg","src_small":"http:\/\/cs316928.userapi.com\/v316928291\/1f49\/Or6S7Or9UGE.jpg","src_xbig":"http:\/\/cs316928.userapi.com\/v316928291\/1f4c\/abHltkXrtwo.jpg","src_xxbig":"http:\/\/cs316928.userapi.com\/v316928291\/1f4d\/2Rr0AtxGd8M.jpg","src_xxxbig":"http:\/\/cs316928.userapi.com\/v316928291\/1f4e\/v3cdEBxHoqY.jpg","width":960,"height":1280,"text":"","created":1340696093}]}
        		attachPhoto(result);
        		break;
        	case 10:
        		clearList = true;
        		offset = 0;
            	onBrowse(-1);
            	break;
        	}
        	
        }
        else {
        }
    }
    
    private void getUpdates() {

    	Intent intent = new Intent(activity, RESTService2.class);
        intent.setData(Uri.parse("http://"+longServer+"?act=a_check&key="+longKey+"&ts="+longTs+"&wait=25&mode=2"));

        intent.putExtra(RESTService2.EXTRA_HTTP_VERB, 0x2);
        intent.putExtra(RESTService2.EXTRA_MODE, 2);
        intent.putExtra(RESTService2.EXTRA_PARAMS, new Bundle());
        intent.putExtra(RESTService2.EXTRA_RESULT_RECEIVER, mReceiver);
        activity.startService(intent);
        Log.d(TAG, "getUpdates");
    }

	public static Intent createIntent(Context context) {
        Intent i = new Intent(context, ActDialog.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }
	
	private static class ProfileAction extends AbstractAction {

        public ProfileAction() {
            super(R.drawable.compose);
        }

        @Override
        public void performAction(View view) {
        	
        }
    }
	
	private void parseUpdates(JSONArray updatesContainer) {
		//06-04 14:02:44.840: D/ActDialog(1228): [[4,48,35,61745456,1338818557," ... ","R",{}]]
		JSONArray updates = null;
		Log.d(TAG, "parseUpdates");
		status.setText("");
		try
        {
			//4,$message_id,$flags,$from_id,$timestamp,$subject,$text,$attachments -- ���������� ������ ��������� 
			//[4,144,33,97768291,1339143358," ... ","Msg from rec",{}]

			for (int i=0;i<updatesContainer.length();i++) {
				updates = updatesContainer.getJSONArray(i);
				int mode = 0;
				mode = updates.getInt(0);
				switch(mode) {
				case 62:
					status.setText(getString(R.string.dlg_typing).replace("{user}", ""));
					
					break;
				case 61:
					status.setText(getString(R.string.dlg_typing).replace("{user}", title.getText().toString()));
					break;
				case 8:
					status.setText(getString(R.string.dlg_seenonline).replace("{user}", title.getText().toString()));
					break;
				case 4:
					//06-15 15:26:57.408: D/ActDialog(8477): [4,216,561,97768291,1339759618," ... ","47",{"attach1_type":"photo","attach1":"97768291_285604904"}]
					if (dlg.getChat_id()!=0) {
						//����� ��������
						clearList = false;
						offset = 0;
	                	onBrowse(-1);
	                	return;
					}
					//read
					int message_id = updates.optInt(1);
					int flags = updates.optInt(2);
					int from_id = updates.optInt(3);
					Date timestamp = new Date( 1000l * updates.getInt(4));
					String subject = updates.optString(5);
					String text = updates.optString(6);
					JSONObject attach = updates.optJSONObject(7);
					Log.d(TAG, "Msg:"+text+" from_id:"+from_id+" dlg.getUid():"+dlg.getUid()+" attach:"+attach.toString());
					//06-13 11:09:04.236: D/ActDialog(28293): Msg:msg from_id:97768291 dlg.getUid():97768291
					//06-13 11:09:04.236: D/ActDialog(28293): [4,157,35,97768291,1339571344," ... ","msg",{}]
					

					int out = 2;
					ClsDialog d = new ClsDialog();
					d.setProfile(null);

						if ((flags & 2)!=0) {
							d.setOut(1);
							d.setUid(dlg.getUid());
						}
						else {
							d.setOut(0);
							d.setUid(AndroidApplication.getPrefsInt("user_id"));
						}	
					//}
					
					//06-13 16:03:46.776: D/ActDialog(32529): [4,171,561,97768291,1339589026," ... ","",{"attach1_type":"photo","attach1":"97768291_285519758"}]
					
		    		
		    		d.setBody(text);
		    		d.setTitle(subject);
		    		d.setMid(message_id);
		    		d.setDate(timestamp);
		    		int index = Collections.binarySearch(dlgs, d, mCmpDialog);
		    		if (index<0) 
	    				dlgs.add(d);//�� ���� ������ - ���������
	    			else{
	    				dlgs.set(index, d);
	    			}
	    			Log.d(TAG, ""+updates.toString());
	    			if (attach!=null && attach.toString().contains("attach")) {//.equals("{}")) {
	    				Log.d(TAG, " attach:"+attach.toString());
	    				clearList = true;
						offset = 0;
	                	onBrowse(-1);	
	    			}
	    			
	    			break;
				}
			}
			
        }
		catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		//update UI
		//Collections.sort(dlgs, mCmpDialog);//������
		adapter.notifyDataSetChanged();
		//if (list.getSelectedItemPosition()==dlgs.size()-2)
			list.setSelection(dlgs.size()-1);
    	//list.startLayoutAnimation();
    	
		//���������� - ��� �� ������ ���������� ������������
		getUpdates();
	}
	
	private void parseResult(String result,boolean useCashe) {
    	Log.d(TAG, "parseresult_dialog"+useCashe+clearList);
    	if (result==null) return;
    	
    	JSONObject mJSONObject = null;
		ClsDialog d;

    	try
        {
    		//"profiles":[{"uid":150882319,"first_name":"�����","last_name":"����","photo":"http:\/\/cs11214.userapi.com\/u150882319\/e_da5fa20c.jpg","online":1,"screen_name":"id150882319","mobile_phone":"790676500**","home_phone":"671-30**"},{"uid":61745456,"first_name":"�������","last_name":"�������","photo":"http:\/\/cs303104.userapi.com\/u61745456\/e_b268652b.jpg","online":0,"screen_name":"id61745456","mobile_phone":"","home_phone":""}]}}
	    	mJSONObject = new JSONObject(result.toString());
	    	mJSONObject = mJSONObject.optJSONObject("response");
	    	JSONArray mDialogs = mJSONObject.optJSONArray("messages");
	    	JSONObject mLastAct = mJSONObject.optJSONObject("activity");
	    	if (mLastAct!=null){
	    		String tmp = title.getText().toString();
	    		if (mLastAct.getInt("online")==1) {
	    			status.setText(getString(R.string.dlg_seenonline).replace("{user}", tmp));
	    		}
	    		else {
	    			Date timestamp = new Date( 1000l * mLastAct.getInt("time"));
	    			status.setText(getString(R.string.dlg_seen).replace("{user}", tmp).replace("{time}", Utils.convertTime(timestamp)));
	    		}
	    	}
	    	//���������������,��������������� �����
			ArrayList<ClsProfile> profiles = new ArrayList<ClsProfile>();
			ClsProfile p;
			Map<String, Integer> profileMap = new HashMap<String, Integer>();
			JSONArray mProfiles = mJSONObject.optJSONArray("profiles");
	    	if (dlg.getChat_id()!=0) {
	    		
				if (mProfiles == null) return;
		    	for (int i=0;i<mProfiles.length();i++) {
		    		mJSONObject = mProfiles.getJSONObject(i);
		    		//"profiles":[{"uid":61745456,"first_name":"�������","mobile_phone":"","last_name":"�������","home_phone":"","screen_name":"id61745456","photo":"http:\/\/cs9531.userapi.com\/u61745456\/e_916e7897.jpg","online":0},{"uid":150882319,"first_name":"�����","mobile_phone":"790676500**","last_name":"����","home_phone":"671-30**","screen_name
		    		p = new ClsProfile();
		    		p.setUid(mJSONObject.optInt("uid"));
			    	p.setFirst_name(mJSONObject.optString("first_name"));
			    	p.setLast_name(mJSONObject.optString("last_name"));
			    	p.setScreen_name(mJSONObject.optString("screen_name"));
			    	p.setPhoto(Uri.decode(mJSONObject.optString("photo_medium")));
			    	p.setOnline(mJSONObject.optInt("online"));
			    	profiles.add(p);
			    	profileMap.put(mJSONObject.optString("uid"), i);
			    	
		    	}
	    	}
	    	if (mDialogs!=null) dlgsSize = mDialogs.optInt(0); else dlgsSize=0;
	    	
	    	if (dlgsSize==0) return;
	    	if (mDialogs==null || mDialogs.length()<=1) return;
	    	if (clearList) dlgs.clear();
	    	for (int i=1;i<mDialogs.length();i++) {
	    		mJSONObject = mDialogs.getJSONObject(i);
	    		d = new ClsDialog();
	    		//{"uid":61745456,"body":"��� ����","title":" ... ","read_state":0,"mid":42,"date":1338209468,"out":1},{"uid":150882319,"body":"��� �� ����?","title":" ... ","read_state":1,"mid":40,"date":1336281089,"out":0}
	    		//"geo":{"type":"point","coordinates":"55.7439492671 37.668416121"}}
	    		//String http = "http://maps.google.com/maps/api/staticmap?center="+lat+","+lon+"&zoom=12&size=400x400&sensor=false&markers=color:blue%7C"+lat+","+lon;
				JSONObject geo = mJSONObject.optJSONObject("geo");
				if (geo!=null) {
					String coord = geo.optString("coordinates").replace(" ", ",");
					if (!coord.equals("")) {
						d.setGeo("http://maps.google.com/maps/api/staticmap?center="+coord+"&zoom=12&size=130x130&sensor=false&markers=color:blue%7C"+coord);
						d.setCoords(coord);
					}
				}
				
	    		d.setUid(mJSONObject.optInt("uid"));
	    		if (dlg.getChat_id()!=0) {
	    			if (profileMap.get(mJSONObject.optString("uid"))!=null)
	    				d.setProfile(profiles.get(profileMap.get(mJSONObject.optString("uid"))));
	    			else 
	    				d.setProfile(null);
	    		}
	    		else {
	    			d.setProfile(null);
	    		}
	    		d.setBody(mJSONObject.optString("body"));
	    		d.setTitle(mJSONObject.optString("title"));
	    		d.setRead_state(mJSONObject.optInt("read_state"));
	    		d.setMid(mJSONObject.optInt("mid"));
	    		////,"fwd_messages":[{"uid":"61745456","date":"1341911485","body":"kk"}]}
	    		JSONArray fwds = mJSONObject.optJSONArray("fwd_messages");
	    		if (fwds!=null) {
	    			String fwdTitle = mJSONObject.optString("body")+" "+getString(R.string.dlg_fwmsg);
	    			String fwdBody = "";
	    			for (int l=0;l<fwds.length();l++) {
	    				JSONObject mFwd = fwds.getJSONObject(l);
	    				fwdBody+="\n";
	    				fwdBody+= mFwd.optString("body");//������ ����
	    			}
	    			d.setBody(fwdTitle+" "+fwdBody);
	    		}
	    		//06-15 15:52:27.198: D/ActDialog(8477): onRESTResult:{"response":{"messages":[46,{"body":"48","mid":217,"uid":97768291,"from_id":97768291,"date":1339761123,"read_state":0,"out":0,
	    		//"attachments":[{"type":"photo","photo":{"pid":285606102,"aid":-3,"owner_id":97768291,"src":"http:\/\/cs304912.userapi.com\/v304912291\/11f7\/3DGjU-eE9sc.jpg","src_big":"http:\/\/cs304912.userapi.com\/v304912291\/11f8\/iuckx-nCodo.jpg","src_small":"http:\/\/cs304912.userapi.com\/v304912291\/11f6\/CtuDFXvPY5o.jpg","src_xbig":"http:\/\/cs304912.userapi.com\/v304912291\/11f9\/40ptYcsxpOQ.jpg","src_xxbig":"http:\/\/cs304912.userapi.com\/v304912291\/11fa\/olWDvKVyzS0.jpg","src_xxxbig":"http:\/\/cs304912.userapi.com\/v304912291\/11fb\/CgmjxHP2DJg.jpg","width":1600,"height":1600,"text":"","created":1339761120,"access_key":"758c3e5bfbbc9bafc6"}},{"type":"photo","photo":{"pid":285606105,"aid":-3,"owner_id":97768291,"src":"http:\/\/cs304912.userapi.com\/v304912291\/1201\/SUEA1QENyto.jpg","src_big":"http:\/\/cs304912.userapi.com\/v304912291\/1202\/jOHN69CJxnA.jpg","src_small":"http:\/\/cs304912.userapi.com\/v304912291\/1200\/G4Dd2XhZeZo.jpg","src_xbig":"http:\/\/cs304912.userapi.com\/v304912291\/1203\/86qYn40GDuc.jpg","src_xxbig":"http:\/\/cs304912.userapi.com\/v304912291\/1204\/7kMDWHWh478.jpg","src_xxxbig":"http:\/\/cs304912.userapi.com\/v304912291\/1205\/oeGZuYwIHg0.jpg","width":1200,"height":1600,"text":"","created":1339761122,"access_key":"a1bfbb5abdfb0144f8"}}]},{"body":"47","mid":216,"uid":97768291,"from_id":97768291,"date":1339759618,"read_state":1,"out":0,"attachment":{"type":"photo","photo":{"pid":285604904,"aid":-3,"owner_id":97768291,"src":"http:\/\/cs304912.userapi.com\/v304912291\/11e6\/rqC1P7773V4.jpg","src_big":"http:\/\/cs304912.userapi.com\/v304912291\/11e7\/eHhJ49kMXHA.jpg","src_small":"http:\/\/cs304912.userapi.com\/v304912291\/11e5\/GQpIfelVqSM.jpg","src_xbig":"http:\/\/cs304912.userapi.com\/v304912291\/11e8\/hm7JEPVmn0I.jpg","src_xxbig":"http:\/\/cs304912.userapi.com\/v304912291\/11e9\/DGitzhQdS2s.jpg","src_xxxbig":"http:\/\/cs304912.userapi.com\/v304912291\/11ea\/1Z8QAWcvIjg.jpg","width":1200,"height":1600,"text":"","created":1339759617,"access_key":"06bfc73f921f0b33e9"}},"attachments":[{"type":"photo","photo":{"pid":285604904,"aid":-3,"owner_id":97768291,"src":"http:\/\/cs304912.userapi.com\/v304912291\/11e6\/rqC1P7773V4.jpg","src_big":"http:\/\/cs304912.userapi.com\/v304912291\/11e7\/eHhJ49kMXHA.jpg","src_small":"http:\/\/cs304912.userapi.com\/v304912291\/11e5\/GQpIfelVqSM.jpg","src_xbig":"http:\/\/cs304912.userapi.com\/v304912291\/11e8\/hm7JEPVmn0I.jpg","src_xxbig":"http:\/\/cs304912.userapi.com\/v304912291\/11e9\/DGitzhQdS2s.jpg","src_xxxbig":"http:\/\/cs304912.userapi.com\/v304912291\/11ea\/1Z8QAWcvIjg.jpg","width":1200,"height":1600,"text":"","created":1339759617,"access_key":"06bfc73f921f0b33e9"}}]},{"body":"46","mid":215,"uid":61745456,"from_id":61745456,"date":1339694714,"read_state":1,"out":1},{"body":"45","mid":214,"uid":61745456,"from_id":61745456,"date":1339694423,"read_state":1,"out":1},{"body":"44","mid":213,"uid":61745456,"from_id":61745456,"date":1339694178,"read_state":1,"out":1},{"body":"43","mid":212,"uid":61745456,"from_id":61745456,"date":1339693901,"read_state":1,"out":1},{"body":"42","mid":211,"uid":97768291,"from_id":97768291,"date":1339693624,"read_state":1,"out":0},{"body":"41","mid":210,"uid":97768291,"from_id":97768291,"date":1339693612,"read_state":1,"out":0},{"body":"40","mid":209,
	    		//"geo":{"type":"point","coordinates":"55.7446080123 37.6299614687"}
	    		//,"attachment":{"type":"video","video":{"vid":162812403,"owner_id":154965609,"title":"���-��-�������(111)","duration":143,"description":"","date":1337857620,"views":1,"image":"http:\/\/cs12479.userapi.com\/u63381559\/video\/m_0ead1eb5.jpg","image_big":"http:\/\/cs12479.userapi.com\/u63381559\/video\/l_cd39669d.jpg","image_small":"http:\/\/cs12479.userapi.com\/u63381559\/video\/s_5434b6e8.jpg","access_key":"7b83b7b5febdfa9652"}}
	    		JSONArray mAttachments = mJSONObject.optJSONArray("attachments");
	    		if (mAttachments!=null) {
	    			//init
	    			ArrayList<ClsAttPhoto> photos = new ArrayList<ClsAttPhoto>();
	    			ArrayList<ClsAttVideo> videos = new ArrayList<ClsAttVideo>();
	    			
	    			for (int j=0;j<mAttachments.length();j++) {
	    				JSONObject mAttach = mAttachments.getJSONObject(j);
	    				if (mAttach.optString("type").equals("photo")) {
	    					JSONObject mPhoto = mAttach.getJSONObject("photo");
	    					
	    					ClsAttPhoto photo = new ClsAttPhoto();
	    					photo.setId(mPhoto.optString("owner_id")+"_"+mPhoto.optString("pid"));
	    					photo.setSrc(Uri.decode(mPhoto.optString("src")));
	    					photo.setSrc_big(Uri.decode(mPhoto.optString("src_big")));
	    					photos.add(photo);
	    				}
	    				if (mAttach.optString("type").equals("video")) {
	    					JSONObject mPhoto = mAttach.getJSONObject("video");
	    					
	    					ClsAttVideo video = new ClsAttVideo();
	    					video.setId(mPhoto.optString("owner_id")+"_"+mPhoto.optString("vid"));
	    					video.setSrc(Uri.decode(mPhoto.optString("image")));
	    					video.setSrc_big(Uri.decode(mPhoto.optString("image_big")));
	    					videos.add(video);
	    				}
	    			}
	    			//add if exist
	    			if (photos.size()>0) d.setAtt_photos(photos);
	    			if (videos.size()>0) d.setAtt_videos(videos);
	    		}
	    		
	    		int out = 1;
	    		if (mJSONObject.optInt("uid")==dlg.getUid()) out = 0;
	    		d.setOut(out);
	    		d.setDate(new Date( 1000l * mJSONObject.getInt("date")));
	    		if (clearList) {
	    			//�������������� ��������
	    			dlgs.add(d);
	    		}
	    		else {
	    			//�������� ����� � ������� (��������� �������, ������ ����� - ����� ���������� � ������� �����������)
	    			int index = Collections.binarySearch(dlgs, d, mCmpDialog);
	    			if (index<0) 
	    				dlgs.add(d);//�� ���� ������ - ���������
	    			else{
	    				dlgs.set(index, d);
	    			}
	    		}
	    	}
        } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	finally {
    		mJSONObject = null;
    	}
    	adapter.notifyDataSetChanged();
    	//list.startLayoutAnimation();
    	list.setSelection(dlgs.size()-1);
    	
    	//if (offset==0) list.setSelection(dlgs.size()-1);
    	if (!useCashe) {
    		//�������� ���
    		Utils.save2cache(result,sig);
    	}
    }
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d(TAG, "onactres "+resultCode+" reqcode "+requestCode);
		if(resultCode==RESULT_CANCELED) return;//TODO samsung check
		if(data!=null && requestCode==CAPTCHA_RESULT){
			
			String code = "var messages = API.messages.send({";//uid:"+AndroidApplication.getPrefsString("user_id")+",message:"+msg+"});" +
	        	//	"return {messages: messages};";
			String codeParams = "";
			Bundle params = data.getExtras().getParcelable(ActCaptcha.EXTRA_CAPTCHA_PARAMS);
			//Bundle newParams = new Bundle();
			//String apiMetod = "";
			for (String key : params.keySet()) {
	            String value = params.get(key).toString();
	            Log.d(TAG, key+":"+value.toString());
	            if (!codeParams.equals("")) codeParams+=",";
	            codeParams+=key+":\""+value+"\"";
			}
			code+=codeParams+"});" + "return {messages: messages};";
			Log.d(TAG, code);
			//start again
			//restRequest(code,false,0);
			Bundle paramss = new Bundle();
		    paramss.putString("code", code);
			restRequest(paramss,2,0,"");
			return;
		}
		if(requestCode==TAKE_PHOTO && !mCurrentPhotoPath.equals("")){
			if (data!=null) {
				Log.d(TAG, data.toString());
			}
			//TODO ����� � ��������� ������� ���� � ������ ������� ��������� ����� ��� � � ������ ������� ��� �� ������ �� ������� ��
			if (!Utils.compressBmp(mCurrentPhotoPath)) {
				//����� �� ����������, �� ������ ���� ����� �����, ����� � �����������. ��������� �� ������ ����� ��������

				//Samsung devices have a bug with no picture result in intent
				//Nice ������! ������, ������!
				final ContentResolver cr = activity.getContentResolver();    
	            final String[] p1 = new String[] {
	                    MediaStore.Images.ImageColumns._ID,
	                    MediaStore.Images.ImageColumns.DATE_TAKEN
	            };                   
	            Cursor c1 = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, p1, null, null, p1[1] + " DESC");     
	            if ( c1.moveToFirst() ) {
	               String uristringpic = "content://media/external/images/media/" +c1.getInt(0);
	               Uri newuri = Uri.parse(uristringpic);
	               //�������� �������� ����������� ������
	               mCurrentPhotoPath =getRealPathFromURI(newuri);
	               //�������
	               if (!Utils.compressBmp(mCurrentPhotoPath))
		            	mCurrentPhotoPath = "";
	            }
	            c1.close();
	            
			}
			if(!mCurrentPhotoPath.equals("")){
            	String code = "var messages = API.photos.getMessagesUploadServer({});" +
    	        		"return {server: messages};";
    			Bundle params = new Bundle();
    		    params.putString("code", code);
    			restRequest(params,2,5,"");
    		}
		}
		if (requestCode==CHOOSE_PHOTO) {
			if(data != null){  
	            Uri selectedImage = data.getData();
	            String[] filePathColumn = {MediaStore.Images.Media.DATA};

	            Cursor cursor = activity.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
	            if (cursor!=null) {cursor.moveToFirst();

	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            mCurrentPhotoPath = cursor.getString(columnIndex);
	            cursor.close();
	            //������� (���� ���� ���)
	            if (!Utils.compressBmp(mCurrentPhotoPath))
	            	mCurrentPhotoPath = "";
	            if(!mCurrentPhotoPath.equals("")){
	            	String code = "var messages = API.photos.getMessagesUploadServer({});" +
	    	        		"return {server: messages};";
	    			Bundle params = new Bundle();
	    		    params.putString("code", code);
	    			restRequest(params,2,5,"");
	    		}  }
	        }
		}
		if(data!=null && requestCode==VOICE_REQUEST){
			ArrayList<String> matches = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
			txt.setText(matches.get(0));
		}
		if (data!=null && requestCode==CHOOSE_LOCATION) {
			String lat = "";
			String lon = "";
			Bundle params = data.getExtras().getParcelable(ActLocation.EXTRA_LOC);
			for (String key : params.keySet()) {
	            String value = params.get(key).toString();
	            Log.d(TAG, key+":"+value.toString());
	            if (key.equals("lat")) lat = ""+Double.parseDouble(value.toString())/1000000;
	            if (key.equals("lon")) lon = ""+Double.parseDouble(value.toString())/1000000;
			}
			String http = "http://maps.google.com/maps/api/staticmap?center="+lat+","+lon+"&zoom=12&size=400x400&sensor=false&markers=color:blue%7C"+lat+","+lon;
			Log.d(TAG, http);
			ClsAttPhoto photo = new ClsAttPhoto();
			photo.setSrc(http);
			photo.setLat(lat);
			photo.setLon(lon);
			photo.setId("");
			attachments.add(photo);
			showAtt();
			//att6.setImageProcessor(new MaskImageProcessor(7));
			//att6.setUrl(http);
			//att6.setVisibility(View.VISIBLE);
			//http://maps.google.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=12&size=400x400&sensor=false&markers=color:blue%7C40.702147,-74.015794
		}
	}
	
	private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(activity, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
	
	protected void onResume() {
        super.onResume();
        paused = false;
      //get longpoll
        Log.d(TAG, "StartLongPoll");
        String code = "var messages = API.messages.getLongPollServer({});" +
        		"return {longpoll: messages};";
        String sig2 = Utils.md5(metod+"&code="+code+ AndroidApplication.getPrefsString("secret"));
        Intent intent = new Intent(activity, RESTService2.class);
        intent.setData(Uri.parse(AndroidApplication.HOST+metod+"&sig="+sig2));
        Bundle params = new Bundle();
        params.putString("code", code);
        intent.putExtra(RESTService2.EXTRA_HTTP_VERB, 0x2);
        intent.putExtra(RESTService2.EXTRA_MODE, 1);
        intent.putExtra(RESTService2.EXTRA_PARAMS, params);
        intent.putExtra(RESTService2.EXTRA_RESULT_RECEIVER, mReceiver);
        activity.startService(intent);
        AndroidApplication.putPrefsInt("current_uid", currentUid);
        
        ClsDialog dlgn = AndroidApplication.getDlg();
        if (dlgn.getChat_id()!=0) {
        	title.setText(dlg.getTitle());
        	multichatcnt.setText(""+dlg.getUsers_count());
        }
    }
	
	protected void onPause() {
        super.onPause();
        AndroidApplication.putPrefsInt("current_uid", 0);
        paused = true;
    }

	private OnScrollListener onScroll = new OnScrollListener() {

        @Override
        public void onScrollStateChanged(AbsListView view,
				int scrollState) {
        	searchAsyncImageViews(view, scrollState == OnScrollListener.SCROLL_STATE_FLING);
        }
        
        private void searchAsyncImageViews(ViewGroup viewGroup, boolean pause) {
	        final int childCount = viewGroup.getChildCount();
	        for (int i = 0; i < childCount; i++) {
	            AsyncImageView image = (AsyncImageView) viewGroup.getChildAt(i).findViewById(R.id.att_photoin);
	            if (image != null) {
	                image.setPaused(pause);
	            }
	            AsyncImageView image2 = (AsyncImageView) viewGroup.getChildAt(i).findViewById(R.id.att_photoout);
	            if (image2 != null) {
	                image2.setPaused(pause);
	            }
	        }
	    }
            
        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (visibleItemCount > 0 && firstVisibleItem + visibleItemCount == totalItemCount &&
            		(offset>0) && !isRunning && offset<dlgsSize) {
            	Log.d(TAG,"onScroll"+dlgsSize+":"+offset);
            	clearList = false;

            	onBrowse(0);
            }
        }
            
    };
    
	private void onBrowse(int mode) {
		
		Log.d(TAG,"onBrowse"+offset+":"+mode);
		if (dlg.getChat_id()!=0) {
		    
		    String code = "var messages = API.messages.getHistory({chat_id:"+dlg.getChat_id()+",count:100,offset:"+offset+"});" +
		    		"var profiles = API.getProfiles({uids: \""+dlg.getChat_active()+"\",fields: \"photo_medium,online,screen_name,contacts\"});" +
            		"return {messages: messages, profiles: profiles};";
	        Bundle params = new Bundle();
	    	params.putString("code", code);
	    	sig = restRequest(params,2,mode,"");
        }
        else {
        	String code = "var messages = API.messages.getHistory({uid:"+dlg.getUid()+",count:100,offset:"+offset+"});" +
        			"var activity =API.messages.getLastActivity({uid:"+dlg.getUid()+"});"+
            		"return {activity: activity,messages: messages};";
        	Bundle params = new Bundle();
    	    params.putString("code", code);
    	    sig = restRequest(params,2,mode,"");
        }
		
        //sig = restRequest(code,useCashe,0);
        
		
    }
	
	public void onAttach(View v) {
		final PopupAction quickAction = new PopupAction(activity,false);
    	quickAction.show(v,false);
    	quickAction.setOnDismissListener(onPopupDismiss);
		quickAction.setAnimStyle(PopupAction.ANIM_GROW_FROM_RIGHT);
		
	}
	
	private OnDismissListener onPopupDismiss = new OnDismissListener() {

    	public void onDismiss() {
    		if (ONDISMISSCODE!=0) attll.setVisibility(View.VISIBLE);
    		
    		switch (ONDISMISSCODE) {
    		case 3:
    			runCamera();
    			ONDISMISSCODE = 0;
    			break;
    		case 4:
    			runGallery();
    			ONDISMISSCODE = 0;
    			break;
    		case 5:
    			runLocation();
    			ONDISMISSCODE = 0;
    			break;
    		}
    		
    	}
    };
    
    public void runLocation() {
    	try {
    		Intent locationPicker = ActLocation.createIntent(this);
			startActivityForResult(locationPicker, CHOOSE_LOCATION);
    	}
    	catch (Exception e) {}
    }
    
    public void runGallery() {
    	try {
    		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
			photoPickerIntent.setType("image/*");
			startActivityForResult(photoPickerIntent, CHOOSE_PHOTO);
    	}
    	catch (Exception e) {}
    }
    
    public void runCamera() {
		try {
			File image = Utils.createImageFile();
			mCurrentPhotoPath = image.getAbsolutePath();
			Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    	takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
	    	Log.d(TAG, "startcamera");
	    	startActivityForResult(takePictureIntent, TAKE_PHOTO);
	    	
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    private void savePhoto(String result) {
    	try {
    		//result:{"server":304912,"photo":"{\"width\":200,\"height\":200}","mid":97768291,"hash":"6a5e8e277563b1eaa37d27f845439c8e","message_code":0,"profile_aid":-6}
			JSONObject mJSONObject = new JSONObject(result);

			Bundle params = new Bundle();
		    params.putString("server", mJSONObject.optString("server"));
		    params.putString("photo", mJSONObject.optString("photo"));
		    params.putString("hash", mJSONObject.optString("hash"));
			restRequest(params,2,9,"photos.saveMessagesPhoto");
		} catch (JSONException e) {
			e.printStackTrace();
		}
    }
    
    private void attachPhoto(String result) {
    	//onRESTResult mode:9 res: {"response":[{"pid":286099476,"id":"photo97768291_286099476","aid":-3,"owner_id":97768291,"src":"http:\/\/cs316928.userapi.com\/v316928291\/1f4a\/uch2lQpFkmg.jpg","src_big":"http:\/\/cs316928.userapi.com\/v316928291\/1f4b\/GzWGeZJdr1A.jpg","src_small":"http:\/\/cs316928.userapi.com\/v316928291\/1f49\/Or6S7Or9UGE.jpg","src_xbig":"http:\/\/cs316928.userapi.com\/v316928291\/1f4c\/abHltkXrtwo.jpg","src_xxbig":"http:\/\/cs316928.userapi.com\/v316928291\/1f4d\/2Rr0AtxGd8M.jpg","src_xxxbig":"http:\/\/cs316928.userapi.com\/v316928291\/1f4e\/v3cdEBxHoqY.jpg","width":960,"height":1280,"text":"","created":1340696093}]}
    	try {
			JSONObject mJSONObject = new JSONObject(result);
			JSONArray mJSONArray = mJSONObject.optJSONArray("response");
    		if (mJSONArray!=null && mJSONArray.length()>0) {
    			mJSONObject = mJSONArray.optJSONObject(0);
    			String src = Uri.decode(mJSONObject.optString("src"));
    			ClsAttPhoto photo = new ClsAttPhoto();
    			photo.setSrc(src);
    			photo.setId(mJSONObject.optString("id"));
    			attachments.add(photo);
    			showAtt();
    		}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
    }
    
    public void onAttPhoto (View v) {
    	runCamera();
    }
    
    public void onAttGallery (View v) {
    	runGallery();
    }
    public void onAttGeo (View v) {
    	runLocation();
    }
	public void onSend(View v) {
		String msg = txt.getText().toString();
		if (msg.equals("") && attachments.size()==0) {
			Animation shake = AnimationUtils.loadAnimation(ActDialog.this, R.anim.shake);
			txt.startAnimation(shake);
			return;
		}
		Bundle params = new Bundle();
		String att="";
		if (attachments.size()>0) {
			//att = ",attachment:\"";
			for (ClsAttPhoto p : attachments) {
				if (!att.equals("")) att+=",";
				if (p.getId().equals("")) {
					if (p.getLat()!=null)
						params.putString("lat", ""+p.getLat());	
					if (p.getLon()!=null)
						params.putString("long", ""+p.getLon());
				}
				else {
					att+=p.getId();
				}
			}
			//att+="\"";
			params.putString("attachment", ""+att);
		}
		if (dlg.getChat_id()!=0) {
			
			params.putString("chat_id", ""+dlg.getChat_id());
		}
		else {
			params.putString("uid", ""+dlg.getUid());	
		}
	    params.putString("message", ""+msg);
		restRequest(params,2,10,"messages.send");
		txt.setText("");
		clearAtt();
		inputManager.hideSoftInputFromWindow(txt.getWindowToken(), 0);

	}
	
	public void onBack(View v) {
		finish();
	}
	
	private void clearAtt() {
		attll.setVisibility(View.GONE);
		attachments = new ArrayList<ClsAttPhoto>();
		hideAtt();
	}
	
	private void hideAtt() {
		att1.setVisibility(View.GONE);
		att2.setVisibility(View.GONE);
		att3.setVisibility(View.GONE);
		att4.setVisibility(View.GONE);
		att5.setVisibility(View.GONE);
		att6.setVisibility(View.GONE);
		del1.setVisibility(View.GONE);
		del2.setVisibility(View.GONE);
		del3.setVisibility(View.GONE);
		del4.setVisibility(View.GONE);
		del5.setVisibility(View.GONE);
		del6.setVisibility(View.GONE);
		att1.clearAnimation();
		att2.clearAnimation();
		att3.clearAnimation();
		att4.clearAnimation();
		att5.clearAnimation();
		att6.clearAnimation();
		if (attachments.size()==0)
			attll.setVisibility(View.GONE);
	}
	private void showAtt() {
		int i = 0;
		for (ClsAttPhoto p : attachments) {
			i++;
			switch(i){
			case 1:
				att1.setImageProcessor(new MaskImageProcessor(7));
    			att1.setUrl(p.getSrc());
    			att1.setVisibility(View.VISIBLE);
    			break;
			case 2:
				att2.setImageProcessor(new MaskImageProcessor(7));
    			att2.setUrl(p.getSrc());
    			att2.setVisibility(View.VISIBLE);
    			break;
			case 3:
				att3.setImageProcessor(new MaskImageProcessor(7));
    			att3.setUrl(p.getSrc());
    			att3.setVisibility(View.VISIBLE);
    			break;
			case 4:
				att4.setImageProcessor(new MaskImageProcessor(7));
    			att4.setUrl(p.getSrc());
    			att4.setVisibility(View.VISIBLE);
    			break;
			case 5:
				att5.setImageProcessor(new MaskImageProcessor(7));
    			att5.setUrl(p.getSrc());
    			att5.setVisibility(View.VISIBLE);
    			break;
			case 6:
				att6.setImageProcessor(new MaskImageProcessor(7));
    			att6.setUrl(p.getSrc());
    			att6.setVisibility(View.VISIBLE);
    			break;
			}
		}
	}
	public void onAtt1 (View v) {
		Animation wobble = AnimationUtils.loadAnimation(ActDialog.this, R.anim.wobble);
		att1.startAnimation(wobble);
		del1.setVisibility(View.VISIBLE);
    }
	public void onAtt2 (View v) {
		Animation wobble = AnimationUtils.loadAnimation(ActDialog.this, R.anim.wobble);
		att2.startAnimation(wobble);
		del2.setVisibility(View.VISIBLE);
    }
	public void onAtt3 (View v) {
		Animation wobble = AnimationUtils.loadAnimation(ActDialog.this, R.anim.wobble);
		att3.startAnimation(wobble);
		del3.setVisibility(View.VISIBLE);
    }
	public void onAtt4 (View v) {
		Animation wobble = AnimationUtils.loadAnimation(ActDialog.this, R.anim.wobble);
		att4.startAnimation(wobble);
		del4.setVisibility(View.VISIBLE);
    }
	public void onAtt5 (View v) {
		Animation wobble = AnimationUtils.loadAnimation(ActDialog.this, R.anim.wobble);
		att5.startAnimation(wobble);
		del5.setVisibility(View.VISIBLE);
    }
	public void onAtt6 (View v) {
		Animation wobble = AnimationUtils.loadAnimation(ActDialog.this, R.anim.wobble);
		att6.startAnimation(wobble);
		del6.setVisibility(View.VISIBLE);
    }
	public void onDel1 (View v) {
		try {
			attachments.remove(0);
		}
		catch (Exception e) {}
		hideAtt();
		showAtt();
	}
	public void onDel2 (View v) {
		try {
			attachments.remove(1);
		}
		catch (Exception e) {}
		hideAtt();
		showAtt();
	}
	public void onDel3 (View v) {
		try {
			attachments.remove(2);
		}
		catch (Exception e) {}
		hideAtt();
		showAtt();
	}
	public void onDel4 (View v) {
		try {
			attachments.remove(3);
		}
		catch (Exception e) {}
		hideAtt();
		showAtt();
	}
	public void onDel5 (View v) {
		try {
			attachments.remove(4);
		}
		catch (Exception e) {}
		hideAtt();
		showAtt();
	}
	public void onDel6 (View v) {
		try {
			attachments.remove(5);
		}
		catch (Exception e) {}
		hideAtt();
		showAtt();
	}
	public void onVoiceBtn (View v) {
		try 
        { 
        	Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        	intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        	intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Voice recognition...");
        	startActivityForResult(intent, VOICE_REQUEST);
        } 
        catch (Exception e) {};
	}
	class MyGestureDetector extends SimpleOnGestureListener {
	    @Override
	    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
	        try {
	            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
	                return false;
	            // right to left swipe
	            if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                //Toast.makeText(ActDialog.this, "Left Swipe", Toast.LENGTH_SHORT).show();
	            	finish();
	            }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	                finish();
	            }
	        } catch (Exception e) {
	            // nothing
	        }
	        return false;
	    }
	}

	public void onUserAvatar(View v) {
		dlg = AndroidApplication.getDlg();
		ClsProfile p = dlg.getProfile();
		ActProfile.name = p.getFirst_name();
		ActProfile.phone = p.getMobile_phone();
		ActProfile.url = p.getPhoto();
		ActProfile.uid = p.getUid();

		startActivity(ActProfile.createIntent(activity));
	}
	public void onMultichat(View v) {
		
		startActivity(ActDialogEdit.createIntent(activity));
	}
	private OnItemClickListener onDlgClick = new AdapterView.OnItemClickListener() 
    {
		@Override
		public void onItemClick(AdapterView<?> av, View arg1,
				int position, long arg3) {
			ClsDialog d = dlgs.get(position);
			d.setSelected(!d.isSelected());
			dlgs.set(position, d);
			boolean hasSelected = false;
			for (ClsDialog dd: dlgs) {
				if (dd.isSelected()) {
					hasSelected = true;
					break;
				}
			}
			if (hasSelected) {
				topMenu.setVisibility(View.GONE);
				subMenu.setVisibility(View.VISIBLE);
			}
			else {
				topMenu.setVisibility(View.VISIBLE);
				subMenu.setVisibility(View.GONE);
			}
		}
    };
    
    public void onSubCancel(View v) {
    	int index = 0;
    	for (ClsDialog dd: dlgs) {
			if (dd.isSelected()) {
				dd.setSelected(false);
				dlgs.set(index, dd);
			}
			index++;
		}
    	adapter.notifyDataSetChanged();
    	topMenu.setVisibility(View.VISIBLE);
		subMenu.setVisibility(View.GONE);
    }
    
    public void onSubDel(View v) {
    	String s = "";
    	int index = 0;
    	SortedMap<String, String> paramsMap = new TreeMap<String, String>();
    	for (ClsDialog dd: dlgs) {
			if (dd.isSelected()) {
				if (!s.equals("")) s+=",";
				s+=""+dd.getMid();
				dd.setSelected(false);
				dlgs.set(index, dd);
				//paramsMap.put(""+index, ""+index);
				//dlgs.remove(index);
			}
			index++;
		}
    	//for (String key : paramsMap.keySet()) {
    		//dlgs.remove(key);
    	//}
    	//adapter.notifyDataSetChanged();
    	topMenu.setVisibility(View.VISIBLE);
		subMenu.setVisibility(View.GONE);
		Bundle params = new Bundle();
	    params.putString("mids", s);
		restRequest(params,2,10,"messages.delete");
		
    }
    public void onFwd(View v) {
    	String s = "";
    	int index = 0;
    	SortedMap<String, String> paramsMap = new TreeMap<String, String>();
    	for (ClsDialog dd: dlgs) {
			if (dd.isSelected()) {
				if (!s.equals("")) s+=",";
				s+=""+dd.getMid();
				dd.setSelected(false);
				dlgs.set(index, dd);
				//paramsMap.put(""+index, ""+index);
				//dlgs.remove(index);
			}
			index++;
		}
    	//for (String key : paramsMap.keySet()) {
    		//dlgs.remove(key);
    	//}
    	//adapter.notifyDataSetChanged();
    	topMenu.setVisibility(View.VISIBLE);
		subMenu.setVisibility(View.GONE);
		Bundle params = new Bundle();
		if (dlg.getChat_id()!=0) {
			params.putString("chat_id", ""+dlg.getChat_id());
		}
		else {
			params.putString("uid", ""+dlg.getUid());	
		}
	    params.putString("forward_messages", s);
		restRequest(params,2,10,"messages.send");
		
    }
}


