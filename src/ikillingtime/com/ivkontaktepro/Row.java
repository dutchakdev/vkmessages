package ikillingtime.com.ivkontaktepro;

import android.view.View;
import ikillingtime.com.ivkontaktepro.R;

public interface Row {
    public View getView(View convertView);
    public int getViewType();
}
