package ikillingtime.com.ivkontaktepro;

import android.view.KeyEvent;
import ikillingtime.com.ivkontaktepro.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;
import utils.serviceloader.RESTService;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


	public class ActContacts extends Fragment {
		private AdpContacts adapter;
		private static final String TAG = "ActContacts";
		private ArrayList<ClsProfile> profiles, savedProfiles;
		private ResultReceiver mReceiver;
		private String sig;
		private Activity activity;
		private TextView friends,online,contacts;
		private Button syncBtn;
		private EditText cntSearch;
		private String metod;
		private boolean isAll = true;
		private ListView list;
		private LinearLayout llContacts,llSinh;
		private int currentTab=0;
		private int synhronized=0;
		
		@Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Log.w(TAG, "create");
            
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

        	View v = inflater.inflate(R.layout.contacts, container, false);
        	friends = (TextView)v.findViewById(R.id.cnt_friends);
        	online = (TextView)v.findViewById(R.id.cnt_online);
        	contacts = (TextView)v.findViewById(R.id.cnt_contacts);
        	cntSearch = (EditText)v.findViewById(R.id.cnt_searchtxt);
        	llContacts = (LinearLayout)v.findViewById(R.id.cnt_searchll);
        	llSinh = (LinearLayout)v.findViewById(R.id.cnt_sinh);
        	syncBtn = (Button)v.findViewById(R.id.cnt_sync);
        	
        	friends.setBackgroundResource(R.drawable.actionbar_background_selected);
        	isAll = true;
        	friends.setOnClickListener(onFriendsClk);
        	online.setOnClickListener(onOnlineClk);
        	contacts.setOnClickListener(onContactsClk);
        	syncBtn.setOnClickListener(onSyncClk);

        	llContacts.setVisibility(View.VISIBLE);
        	llSinh.setVisibility(View.GONE);
            
            activity = getActivity();
            profiles = new ArrayList<ClsProfile>();
            adapter = new AdpContacts(getActivity(),profiles,false);

            metod = "/method/execute?access_token="+AndroidApplication.getPrefsString("access_token");
            
            list = (ListView) v.findViewById(R.id.list);
            list.setVisibility(View.VISIBLE);
            Log.d(TAG, "createView");
            list.setAdapter(adapter);
            list.setOnItemClickListener(onContactClick);
            AnimationSet set = new AnimationSet(true);
            Animation animation = new AlphaAnimation(0.0f, 1.0f);
            animation.setDuration(400);
            set.addAnimation(animation);
            animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
            );
            animation.setDuration(400);
            set.addAnimation(animation);

            LayoutAnimationController controller =
                new LayoutAnimationController(set, 0.25f);
            list.setLayoutAnimation(controller);
            
            mReceiver = new ResultReceiver(new Handler()) {

                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    if (resultData != null && resultData.containsKey("REST_RESULT")) {
                        onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                    }
                    else {
                        onRESTResult(resultCode, null, 0);
                    }
                    
                }
                
            };
            
            String code = "var profiles = API.friends.get({fields: \"photo_medium,photo,online,screen_name,contacts\",order:\"hints\"});" +
            		"return {profiles: profiles};";
            currentTab=0;
            sig = restRequest(code,0);
            cntSearch.setOnKeyListener(onSchTxt);
            cntSearch.setText("");
            //TODO ?? apply filters on fly or reset?
            cntSearch.addTextChangedListener(new TextWatcher(){
        	    public void afterTextChanged(Editable s){
        	    	Log.d(TAG, s.toString());
        	        filterByName(s.toString());
        	    }
        	    public void beforeTextChanged(CharSequence s, int start, int count, int after){
        	    	saveProfiles(s.toString());
        	    }
        	    public void onTextChanged(CharSequence s, int start, int before, int count){ }
        	});
            return v;
        }

        private View.OnKeyListener onSchTxt = new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            EditText edt = (EditText)v.findViewById(R.id.cnt_searchtxt);
                            //Log.d(TAG, "SCH:"+edt.getText().toString());

                            String code="var profiles = API.users.search({q: \""+edt.getText().toString()+"\",fields: \"photo_medium,photo,online,screen_name,contacts\",count:1000});" +
                                    "return {profiles: profiles};";
                            sig = restRequest(code,3);
                            return true;
                        default:
                            break;
                    }
                }
                return false;

            }

        };

        private void onRESTResult(int code, String result, int mode) {

            if (code == 200 && result != null) {
            	Log.d(TAG, "onRESTResult:"+currentTab+" m:"+mode+":"+result);
            	if (result.contains("error_code\":15")) {
            		Toast.makeText(activity, "Access denied: You must validate your mobile number", Toast.LENGTH_LONG).show();
    		        return;
            	}
                //adapter = new AdpContacts(getActivity(),profiles);
            	switch (mode) {
            	case 0:
            		if (currentTab==mode)
            			parseResult(result,false);//��������
            		break;
            	case 1:
            		if (currentTab==mode)
            			parseResult(result,false);//��������
            		break;
            	case 2:
            		if (currentTab==mode)
            			parseSynh(result);
            		synhronized=1;
            		break;
                case 3:
                    parseResult(result,true);
                    break;
            	}
            }
        }
        
        private void parseSynh(String result) {
        	
	    	Log.d(TAG, "parsesynh");
	    	cntSearch.setText("");
            
	    	if (result==null) return;

        	JSONObject mJSONObject = null;

			ClsProfile p;
	    	try
	        {
		    	mJSONObject = new JSONObject(result.toString());
		    	//mJSONObject = mJSONObject.optJSONObject("response");
		    	JSONArray mProfiles = mJSONObject.optJSONArray("response");
		    	
		    	if (mProfiles == null) return;
		    	for (int i=0;i<mProfiles.length();i++) {
		    		mJSONObject = mProfiles.getJSONObject(i);
		    		//"profiles":[{"uid":61745456,"first_name":"�������","mobile_phone":"","last_name":"�������","home_phone":"","screen_name":"id61745456","photo":"http:\/\/cs9531.userapi.com\/u61745456\/e_916e7897.jpg","online":0},{"uid":150882319,"first_name":"�����","mobile_phone":"790676500**","last_name":"����","home_phone":"671-30**","screen_name
		    		String phone = mJSONObject.optString("phone");
		    		if (phone.equals(""))
		    			continue;
		    		Log.d(TAG, phone);
		    		int index=0;
		    		for(ClsProfile old: profiles) {
		    			if (old.getMobile_phone().equals(phone)){
		    				//������� ���� �����, ������!
		    				p = new ClsProfile();
				    		p.setInd_name(mJSONObject.optString("first_name").length()>0?mJSONObject.optString("first_name").substring(0, 1):"");
					    	p.setFirst_name(mJSONObject.optString("first_name"));
					    	p.setLast_name(mJSONObject.optString("last_name"));
					    	p.setScreen_name(mJSONObject.optString("screen_name"));
					    	p.setPhoto(Uri.decode(AndroidApplication.isLow()?mJSONObject.optString("photo"):mJSONObject.optString("photo_medium")));
					    	p.setOnline(mJSONObject.optInt("online"));
					    	p.setUid(mJSONObject.optInt("uid"));
					    	p.setMobile_phone(phone);
					    	profiles.set(index, p);
		    			}
		    			index++;
		    		}
		    	}
		    	Utils.save2cache(result,sig);
	        }
	    	catch (Exception e)
	        {
	    		Log.e(TAG, e.toString());
	        }
	    	finally {
	    		mJSONObject = null;
	    	}
	    	Log.d(TAG, "size"+profiles.size());
	    	Collections.sort(profiles, new Comparator<ClsProfile>() {
	            @Override
	            public int compare(ClsProfile p1, ClsProfile p2) {
	            	String s1 = p1.getInd_name();
	            	String s2 = p2.getInd_name();
	                return s1.compareToIgnoreCase(s2);
	            }
	        });

	    	adapter = new AdpContacts(getActivity(),profiles,false);
            list.setAdapter(adapter);
	    	adapter.notifyDataSetChanged();
	    }
        
        private void parseResult(String result,boolean isSearch) {
        	
	    	Log.d(TAG, "parseresult"+isAll);
	    	cntSearch.setText("");
	    	profiles = new ArrayList<ClsProfile>();
            
	    	if (result==null) return;

        	JSONObject mJSONObject = null;

			ClsProfile p;
	    	try
	        {
		    	mJSONObject = new JSONObject(result.toString());
		    	mJSONObject = mJSONObject.optJSONObject("response");
		    	JSONArray mProfiles = mJSONObject.optJSONArray("profiles");
		    	
		    	if (mProfiles == null) return;
                int startFrom = 0;
                if (isSearch) {
                    startFrom = 1;
                }
		    	for (int i=startFrom;i<mProfiles.length();i++) {
		    		mJSONObject = mProfiles.getJSONObject(i);
		    		//"profiles":[{"uid":61745456,"first_name":"�������","mobile_phone":"","last_name":"�������","home_phone":"","screen_name":"id61745456","photo":"http:\/\/cs9531.userapi.com\/u61745456\/e_916e7897.jpg","online":0},{"uid":150882319,"first_name":"�����","mobile_phone":"790676500**","last_name":"����","home_phone":"671-30**","screen_name
		    		
		    		p = new ClsProfile();
		    		if (i<=4) p.setInd_name("");
		    		else {
		    			if (mJSONObject.optString("first_name").length()>0)
		    				p.setInd_name(mJSONObject.optString("first_name").substring(0, 1));
		    			else p.setInd_name("");
		    		}
			    	p.setFirst_name(mJSONObject.optString("first_name"));
			    	p.setLast_name(mJSONObject.optString("last_name"));
			    	p.setScreen_name(mJSONObject.optString("screen_name"));
			    	p.setPhoto(Uri.decode(AndroidApplication.isLow()?mJSONObject.optString("photo"):mJSONObject.optString("photo_medium")));
			    	p.setOnline(mJSONObject.optInt("online"));
			    	p.setUid(mJSONObject.optInt("uid"));
			    	if (isAll) {
		    			//�������������� ��������
			    		profiles.add(p);
		    		}
		    		else {
		    			if (mJSONObject.optInt("online")==1) {
		    				profiles.add(p);
		    			}
		    			
		    		}
		    	}
	        }
	    	catch (Exception e)
	        {
	    		Log.e(TAG, e.toString());
	        }
	    	finally {
	    		mJSONObject = null;
	    	}
	    	Log.d(TAG, "size"+profiles.size());
	    	Collections.sort(profiles, new Comparator<ClsProfile>() {
	            @Override
	            public int compare(ClsProfile p1, ClsProfile p2) {
	            	String s1 = p1.getInd_name();
	            	String s2 = p2.getInd_name();
	                return s1.compareToIgnoreCase(s2);
	            }
	        });

	    	adapter = new AdpContacts(getActivity(),profiles,false);
            list.setAdapter(adapter);
	    	adapter.notifyDataSetChanged();

	    }
        
        private OnItemClickListener onContactClick = new AdapterView.OnItemClickListener() 
	    {
			@Override
			public void onItemClick(AdapterView<?> av, View arg1,
					int position, long arg3) {
				ClsProfile p = profiles.get(position);
				if (p.getUid()<0) {
					ActProfile.name = p.getFirst_name();
					ActProfile.phone = p.getMobile_phone();
					ActProfile.url = p.getPhoto();
					ActProfile.uid = p.getUid();
					startActivity(ActProfile.createIntent(activity));
					return;
				}
				if (currentTab==2){
					ActProfile.name = p.getFirst_name();
					ActProfile.phone = p.getMobile_phone();
					ActProfile.url = p.getPhoto();
					ActProfile.uid = p.getUid();
					ClsDialog d = new ClsDialog();
					d.setUid(p.getUid());
					d.setProfile(p);
					AndroidApplication.setDlg(d);
					startActivity(ActProfile.createIntent(activity));
					return;
				}
				if (AndroidApplication.getChooseContact()==0) {
					
					ClsDialog d = new ClsDialog();
					d.setUid(p.getUid());
					d.setProfile(p);
					AndroidApplication.setDlg(d);
					startActivity(ActDialog.createIntent(getActivity()));
				}
				else {
					AndroidApplication.setChooseContact(0);
					AndroidApplication.setChooseContactId(p.getUid());
					startActivity(ActDialogEdit.createIntent(activity));	
				}
				
			}
	    };
	    
	    private OnClickListener onOnlineClk = new View.OnClickListener() 
	    {
			@Override
			public void onClick(View v) {
				currentTab=1;
				cntSearch.setText("");
				isAll = false;
				list.setVisibility(View.VISIBLE);
				//list.startLayoutAnimation();
				llContacts.setVisibility(View.VISIBLE);
				llSinh.setVisibility(View.GONE);
				String code = "var profiles = API.friends.get({fields: \"photo_medium,photo,online,screen_name,contacts\",order:\"hints\"});" +
	            		"return {profiles: profiles};";
	            sig = restRequest(code,1);
				online.setBackgroundResource(R.drawable.actionbar_background_selected);
				friends.setBackgroundResource(R.drawable.actionbar_contacts);
				contacts.setBackgroundResource(R.drawable.actionbar_contacts);
			}
	    };
	    private OnClickListener onFriendsClk = new View.OnClickListener() 
	    {
			@Override
			public void onClick(View v) {
				currentTab=0;
				cntSearch.setText("");
				isAll = true;
				list.setVisibility(View.VISIBLE);
				list.startLayoutAnimation();
				llContacts.setVisibility(View.VISIBLE);
				llSinh.setVisibility(View.GONE);
				String code = "var profiles = API.friends.get({fields: \"photo_medium,photo,online,screen_name,contacts\",order:\"hints\"});" +
	            		"return {profiles: profiles};";
	            sig = restRequest(code,0);
				friends.setBackgroundResource(R.drawable.actionbar_background_selected);
				online.setBackgroundResource(R.drawable.actionbar_contacts);
				contacts.setBackgroundResource(R.drawable.actionbar_contacts);
			}
	    };
	    private OnClickListener onContactsClk = new View.OnClickListener() 
	    {
			@Override
			public void onClick(View v) {
				currentTab=2;
				contacts.setBackgroundResource(R.drawable.actionbar_background_selected);
				friends.setBackgroundResource(R.drawable.actionbar_contacts);
				online.setBackgroundResource(R.drawable.actionbar_contacts);
				
				StringBuilder result = new StringBuilder("");
				profiles = new ArrayList<ClsProfile>();
				//����� �����������
				try {
					int index = 0;
					Cursor phones = activity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
					while (phones.moveToNext())
					{
						String name=""+phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
						String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

						ClsProfile p = new ClsProfile();
						p.setInd_name(name.length()>0?name.substring(0, 1):"");
				    	p.setFirst_name(name);
				    	p.setLast_name("");
				    	p.setScreen_name("");
				    	p.setPhoto("");
				    	p.setOnline(0);
				    	p.setMobile_phone(phoneNumber);
				    	p.setUid(-1);
				    	profiles.add(p);
					    if (index==1000) break;
					    if (index!=0) result.append(",");
					    result.append(phoneNumber);
					    index++;
					}
					phones.close();
				}
				catch (Exception e) {}
				cntSearch.setText("");
				
				Collections.sort(profiles, new Comparator<ClsProfile>() {
		            @Override
		            public int compare(ClsProfile p1, ClsProfile p2) {
		            	String s1 = p1.getInd_name();
		            	String s2 = p2.getInd_name();
		                return s1.compareToIgnoreCase(s2);
		            }
		        });

		    	adapter = new AdpContacts(getActivity(),profiles,false);
	            list.setAdapter(adapter);
		    	adapter.notifyDataSetChanged();
		    	
				list.setVisibility(View.VISIBLE);
				list.startLayoutAnimation();
				llContacts.setVisibility(View.VISIBLE);
				llSinh.setVisibility(View.GONE);
				
				//�������������� ��� ������ ������ ������� ���������
				
				if (result.toString().equals("")) {
					//Toast.makeText(activity, "Error sync contacts..", Toast.LENGTH_SHORT).show();
					//���� �� �� ������ ������� �����
					return;
				}
				//String q = "https://api.vk.com/method/friends.getByPhones?fields="+"photo,online,screen_name,contacts"+"&phones="+
	    			//	Uri.encode(result.toString())+"&access_token="+AndroidApplication.getPrefsString("access_token");
				Bundle params = new Bundle();
			    params.putString("fields", "photo_medium,photo,online,screen_name,contacts");
			    params.putString("phones", result.toString());
			    sig = restRequestHttps(params,2,2,"friends.getByPhones");
			    
				/*cntSearch.setText("");
				list.setVisibility(View.GONE);
				llContacts.setVisibility(View.GONE);
				llSinh.setVisibility(View.VISIBLE);
				contacts.setBackgroundResource(R.drawable.actionbar_background_selected);
				friends.setBackgroundResource(R.drawable.actionbar_contacts);
				online.setBackgroundResource(R.drawable.actionbar_contacts);*/
			}
	    };
	    private OnClickListener onSyncClk = new View.OnClickListener() 
	    {
			@Override
			public void onClick(View v) {
				StringBuilder result = new StringBuilder("");
				//����� �����������
				try {
					int index = 0;
					Cursor phones = activity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
					while (phones.moveToNext())
					{
					  String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
					  String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					  Log.d(TAG,name+":"+phoneNumber);
					  if (index==1000) break;
					  if (index!=0) result.append(",");
					  result.append(phoneNumber);
					  index++;
					}
					phones.close();
				}
				catch (Exception e) {}
				if (result.toString().equals("")) {
					Toast.makeText(activity, "Error sync contacts..", Toast.LENGTH_SHORT).show();
					//���� �� �� ������ ������� �����
					return;
				}
				//String q = "https://api.vk.com/method/friends.getByPhones?fields="+"photo,online,screen_name,contacts"+"&phones="+
	    			//	Uri.encode(result.toString())+"&access_token="+AndroidApplication.getPrefsString("access_token");
				Bundle params = new Bundle();
			    params.putString("fields", "photo_medium,photo,online,screen_name,contacts");
			    params.putString("phones", result.toString());
			    restRequestHttps(params,2,2,"friends.getByPhones");

			}
	    };
	    
	    private String restRequestHttps(Bundle params,int httpVerb,int mode,String method) {
	    	//progressBar.setVisibility(View.VISIBLE);
	    	StringBuilder paramsString = new StringBuilder();
	    	SortedMap<String, String> paramsMap = new TreeMap<String, String>();
	    	for (String key : params.keySet()) {
	    		paramsMap.put(key, params.get(key).toString());
	    	}

	    	for (String key : paramsMap.keySet()) {
	            paramsString.append("&");
	            paramsString.append(key);
	            paramsString.append("=");
	            paramsString.append(paramsMap.get(key).toString());
	    	}
	    	if (method.equals("")) method = "execute";
	    	String methodString = "/method/"+method+"?access_token="+AndroidApplication.getPrefsString("access_token");
			String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));
			if (Utils.loadFromCashe(sig)!=null) {
				parseSynh(Utils.loadFromCashe(sig));
				return "";
			}
			//Log.d(TAG, methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));
	        Intent intent = new Intent(activity, RESTService.class);
	        intent.setData(Uri.parse("https://api.vk.com"+methodString+"&sig="+sig));
	        intent.putExtra(RESTService.EXTRA_MODE, mode);	        
	        intent.putExtra(RESTService.EXTRA_HTTP_VERB, httpVerb);
	        intent.putExtra(RESTService.EXTRA_PARAMS, params);
	        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
	        activity.startService(intent);
	        return sig;
		}
	    
	    private String restRequest(String code,int mode) {
			String sig = Utils.md5(metod+"&code="+code+ AndroidApplication.getPrefsString("secret"));
	        
	        //parseResult(Utils.loadFromCashe(sig),true);
	        
	        Intent intent = new Intent(activity, RESTService.class);
	        intent.setData(Uri.parse(AndroidApplication.HOST+metod+"&sig="+sig));
	        Bundle params = new Bundle();
	        params.putString("code", code);
	        intent.putExtra(RESTService.EXTRA_HTTP_VERB, 0x2);
	        intent.putExtra(RESTService.EXTRA_PARAMS, params);
	        intent.putExtra(RESTService.EXTRA_MODE, mode);
	        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
	        activity.startService(intent);
	        return sig;
		}
	    
	    private void filterByName (String name) {
	    	if (name.trim().equals("")) {
	    		profiles = savedProfiles;
	    		adapter = new AdpContacts(getActivity(),profiles,false);
	            list.setAdapter(adapter);
		    	adapter.notifyDataSetChanged();
		    	return;
	    	}
	    	if (profiles==null) return;
	    	if (profiles.size()==0) return;
	    	ArrayList<ClsProfile> oldProfiles = profiles;
	    	ArrayList<ClsProfile> newProfiles = new ArrayList<ClsProfile>();
	    	for (ClsProfile p: profiles) {
	    		String fullName = p.getFirst_name().toLowerCase()+p.getLast_name().toLowerCase()+p.getScreen_name().toLowerCase();
	    		if (fullName.contains(name)) {
	    			newProfiles.add(p);
	    		}
	    	}
	    	profiles = newProfiles;
	    	adapter = new AdpContacts(getActivity(),profiles,false);
            list.setAdapter(adapter);
	    	adapter.notifyDataSetChanged();
	    }
	    
	    private void saveProfiles (String name) {
	    	if (name.trim().equals("")) {
	    		savedProfiles = profiles;
	    	}
	    }
    }
//}