package ikillingtime.com.ivkontaktepro;

import android.app.AlertDialog;
import android.content.DialogInterface;
import ikillingtime.com.ivkontaktepro.R;
import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;

import utils.serviceloader.RESTService;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

public class ActVideo extends Activity {
    
	private static final String TAG = "ActVideo";
	public static String id = "";
    public static String access_key = "";
	private ResultReceiver mReceiver;
	private Activity activity;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
    	mReceiver = new ResultReceiver(new Handler()) {
             @Override
             protected void onReceiveResult(int resultCode, Bundle resultData) {
                 if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                     //onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                     onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                 }
                 else {
                     onRESTResult(resultCode, null,0);
                 }
             }
             
        };
        
        Bundle params = new Bundle();

        Log.w(TAG,id);
	    params.putString("videos", id);

		restRequest(params,2,0,"video.get");
    }
	
	public void onRESTResult(int code, String result, int mode) {
		Log.d(TAG, "onRESTResult:"+result);
		if (code == 200 && result != null) {
        	
			if (result.contains("error_msg")) {
 				try {
 					JSONObject o = new JSONObject(result);
 					o = o.optJSONObject("error");
 					Toast.makeText(this, "Error:"+o.optString("error_msg"), Toast.LENGTH_LONG).show();
 				}
 				catch (Exception e) {}
 				return;
 			}
        	switch (mode) {
        	case 0:
        		try {
        			//{"response":[1,{"vid":162812403,"owner_id":154965609,"title":"���-��-�������(111)","description":"","duration":143,"link":"video162812403","image":"http:\/\/cs12479.userapi.com\/u63381559\/video\/m_0ead1eb5.jpg","image_medium":"http:\/\/cs12479.userapi.com\/u63381559\/video\/l_cd39669d.jpg","date":1337857620,"views":1,
    	        	//"files":{"mp4_240":"http:\/\/cs12479.userapi.com\/u63381559\/video\/5ef88a39a8.240.mp4","mp4_360":"http:\/\/cs12479.userapi.com\/u63381559\/video\/5ef88a39a8.360.mp4","mp4_480":"http:\/\/cs12479.userapi.com\/u63381559\/video\/5ef88a39a8.480.mp4"},"player":"http:\/\/vk.com\/video_ext.php?oid=154965609&id=162812403&hash=dc730fa72c224db4"}]}
    	        	JSONObject mJSONObject = new JSONObject(result);
    	        	
    	        	JSONArray arr = mJSONObject.optJSONArray("response");


                    mJSONObject = arr.getJSONObject(1);
                    mJSONObject = mJSONObject.optJSONObject("files");
                    String url = "";
                    if (AndroidApplication.getPrefsInt("max_quality",0)==1){
                        url = Uri.decode(mJSONObject.optString("mp4_720"));
                    }
                    else {
                        url = Uri.decode(mJSONObject.optString("mp4_480"));
                    }
                    if (url.equals(""))
                        url = mJSONObject.optString("mp4_360");
                    if (url.equals(""))
                        url = mJSONObject.optString("mp4_240");
                    if (url.equals("")) {
                        url = mJSONObject.optString("external");
                        if (!url.equals("")) {
                            startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(url)));
                        }
                        else {
                            url = Uri.decode(mJSONObject.optString("flv_320"));
                            try {
                                Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                                Uri data = Uri.parse(url);
                                intent.setDataAndType(data, "video/*");
                                startActivity(intent);
                            }
                            catch (Exception e) {
                                Toast.makeText(ActVideo.this, e.getMessage(), Toast.LENGTH_LONG).show();

                            }
                        }
                    }
                    else {
                        try {
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                            Uri data = Uri.parse(url);
                            intent.setDataAndType(data, "video/*");
                            startActivity(intent);
                        }
                        catch (Exception e) {
                            Toast.makeText(ActVideo.this, e.getMessage(), Toast.LENGTH_LONG).show();

                        }
                    }
                    finish();

        		}
        		catch (Exception e) {
                    AlertDialog.Builder bld  = new AlertDialog.Builder(activity);
                    /*bld.setPositiveButton(getString(R.string.vid_actlist4),new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            Bundle params = new Bundle();
                            try {
                                String[] s = id.split("_");
                                params.putString("oid", s[0]);
                                params.putString("vid", s[1]);

                            }
                            catch (Exception e) {
                                Log.e(TAG,e.toString());
                            }
                            restRequest(params,2,2,"video.add");
                            finish();
                        }
                    }); */
                    bld.setNegativeButton(getString(R.string.sgn_cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            finish();
                        }
                    });
                    bld.setMessage(getString(R.string.vid_private)).setCancelable(true).create().show();
                }
        		break;
        	}
        }
	}
	
	private String restRequest(Bundle params,int httpVerb,int mode,String method) {
    	StringBuilder paramsString = new StringBuilder();
    	SortedMap<String, String> paramsMap = new TreeMap<String, String>();
    	for (String key : params.keySet()) {
    		paramsMap.put(key, params.get(key).toString());
    	}

    	for (String key : paramsMap.keySet()) {
            paramsString.append("&");
            paramsString.append(key);
            paramsString.append("=");
            paramsString.append(paramsMap.get(key).toString());
    	}
    	if (method.equals("")) method = "execute";
    	String methodString = "/method/"+method+"?access_token="+AndroidApplication.getPrefsString("access_token");
		String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));

		//Log.d(TAG, methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));
        Intent intent = new Intent(activity, RESTService.class);
        intent.setData(Uri.parse(AndroidApplication.HOST+methodString+"&sig="+sig));
        intent.putExtra(RESTService.EXTRA_MODE, mode);
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, httpVerb);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
        activity.startService(intent);
        return sig;
	}
    
    
    public static Intent createIntent(Context context) {
        Intent i = new Intent(context, ActVideo.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }
}