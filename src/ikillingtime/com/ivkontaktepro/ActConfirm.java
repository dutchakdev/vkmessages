package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import org.json.JSONObject;
import utils.serviceloader.RESTService;

import java.util.SortedMap;
import java.util.TreeMap;

public class ActConfirm extends Activity {
    
	private static final String TAG = "C2DMReceiver";
	private ResultReceiver mReceiver;
	private EditText phone,name,lastname;
	private ImageView phoneimg,nameimg,lastnameimg; 
	public static String phoneNumber;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.confirm);
        phone = (EditText) findViewById(R.id.singup_phone);
        name = (EditText) findViewById(R.id.singup_name);
        
        

        mReceiver = new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                    //onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                    onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                }
                else {
                    onRESTResult(resultCode, null,0);
                }
            }
            
        };
        
    }

	public void onRESTResult(int code, String result, int mode) {
     	Log.d(TAG, "onRESTResult:"+result);
     	if (code==200) {
     		if (mode==0) {
     			if (result.contains("error_msg")) {
     				try {
     					JSONObject o = new JSONObject(result);
     					o = o.optJSONObject("error");
     					Toast.makeText(this, "Error:"+o.optString("error_msg"), Toast.LENGTH_LONG).show();
     				}
     				catch (Exception e) {}
     				return;
     			}
     			//no error
     			Toast.makeText(this, R.string.cnf_success, Toast.LENGTH_LONG).show();
     			startActivity(ActLogin.createIntent(ActConfirm.this)); 
     		}
     	}
	}
	
	private String restRequest(Bundle params,Boolean useCashe,int httpVerb,int mode,String method) {

    	StringBuilder paramsString = new StringBuilder();
    	SortedMap<String, String> paramsMap = new TreeMap<String, String>();
    	for (String key : params.keySet()) {
    		paramsMap.put(key, params.get(key).toString());
    	}

    	for (String key : paramsMap.keySet()) {
            paramsString.append("&");
            paramsString.append(key);
            paramsString.append("=");
            paramsString.append(paramsMap.get(key).toString());
    	}
    	if (method.equals("")) method = "execute";
    	String methodString = "/method/"+method;//+"?access_token="+AndroidApplication.getPrefsString("access_token");
		//String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));
		
        Intent intent = new Intent(this, RESTService.class);
        intent.setData(Uri.parse("https://api.vk.com/"+methodString));//+"&sig="+sig));
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, httpVerb);
        intent.putExtra(RESTService.EXTRA_MODE, mode);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
        startService(intent);
        return "";
	}
	
    public static Intent createIntent(Context context) {
        Intent i = new Intent(context, ActConfirm.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }

	
	public void onRegister(View v) {
		if (name.getText().toString().trim().length()<6) {
    		Animation shake = AnimationUtils.loadAnimation(ActConfirm.this, R.anim.shake);
    		name.startAnimation(shake);
    		Toast.makeText(this, R.string.cnf_passerr, Toast.LENGTH_LONG).show();
    		return;
    	}
		Bundle params = new Bundle();
	    params.putString("phone", phoneNumber);
	    params.putString("code", phone.getText().toString().trim());
	    params.putString("password", name.getText().toString());
	    params.putString("client_id", "2965041");
	    params.putString("client_secret", "fXK28HAI0nVRK3hNZiGs");
	    //params.putString("test_mode", "1");//dont forget comment this, Luke!
    	restRequest(params,false,2,0,"auth.confirm");	
	}

}