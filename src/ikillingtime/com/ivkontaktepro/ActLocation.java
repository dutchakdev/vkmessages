package ikillingtime.com.ivkontaktepro;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.maps.*;


public class ActLocation extends MapActivity implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener
{    
	public static final String EXTRA_LOC          = "EXTRA_LOC";
	MapView mapView;
    MyLocationOverlay myLocationOverlay;
    ClsOverlays itemizedoverlay;
    GestureDetector gestureDetector = null;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location);

        mapView = (MapView) findViewById(R.id.mapView);
        MapController mapController = mapView.getController();
		mapController.setZoom(15);
		//mapView.setBuiltInZoomControls(true);
		mapView.setClickable(true);
		Drawable drawable = this.getResources().getDrawable(R.drawable.map_pin);
		
        myLocationOverlay = new MyLocationOverlay(this, mapView);
        mapView.getOverlays().add(myLocationOverlay);
        myLocationOverlay.runOnFirstFix(new Runnable() {
        	public void run() {
        		mapView.getController().animateTo(myLocationOverlay.getMyLocation());
        	 	}
        }); 
        

		itemizedoverlay = new ClsOverlays(drawable,this);
		OverlayItem overlayitem = new OverlayItem(mapView.getMapCenter(), "", "");
	    itemizedoverlay.addOverlay(overlayitem);
	    mapView.getOverlays().add(itemizedoverlay);
		mapView.invalidate();
		
		gestureDetector = new GestureDetector(this);
        gestureDetector.setOnDoubleTapListener(this);

    }

    public static Intent createIntent(Context context) {
        Intent i = new Intent(context, ActLocation.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
        return i;
    }
 

    @Override
	protected void onResume() {
		super.onResume();
		myLocationOverlay.enableMyLocation();
		myLocationOverlay.enableCompass();
	}

	@Override
	protected void onPause() {
		super.onPause();
		myLocationOverlay.disableMyLocation();
		myLocationOverlay.disableCompass();
	}

	public void onChoose(View v) {
		GeoPoint p = mapView.getMapCenter();
		Intent intent= new Intent(this, ActLocation.class);
		Bundle params = new Bundle();
		params.putString("lon", ""+p.getLongitudeE6());
		params.putString("lat", ""+p.getLatitudeE6());
    	intent.putExtra(EXTRA_LOC, params);
    	setResult(RESULT_OK, intent);
    	finish();
	}
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        gestureDetector.onTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }
    @Override
    public boolean onDoubleTap(MotionEvent me) {
    	GeoPoint p = mapView.getProjection().fromPixels(
                (int) me.getX(),
                (int) me.getY());
       
        Drawable drawable = ActLocation.this.getResources().getDrawable(R.drawable.map_pin);
    	itemizedoverlay = new ClsOverlays(drawable,ActLocation.this);
		OverlayItem overlayitem = new OverlayItem(p, "", "");
	    itemizedoverlay.addOverlay(overlayitem);
	    mapView.getOverlays().clear();
	    mapView.getOverlays().add(itemizedoverlay);
	    mapView.getController().animateTo(p);
        mapView.getController().zoomIn();
		mapView.invalidate();
        return true;
    }
  //Overridden methods but not used
    @Override
    public boolean onDoubleTapEvent(MotionEvent me) {
        return false;
    }

    @Override
    public boolean onDown(MotionEvent me) {
        return false;
    }

    @Override
    protected boolean isRouteDisplayed() {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent me) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    @Override
    public boolean onSingleTapUp(MotionEvent me) {
        return false;
    }


}