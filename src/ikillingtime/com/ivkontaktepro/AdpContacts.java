package ikillingtime.com.ivkontaktepro;

import ikillingtime.com.ivkontaktepro.R;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import utils.imageloader.AsyncImageView;
import utils.imageloader.ImageProcessor;
import utils.imageloader.MaskImageProcessor;
import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

public class AdpContacts extends BaseAdapter implements SectionIndexer{
    
	private static final String TAG = "AdpContacts";
    private Activity activity;
    private ArrayList<ClsProfile> data;
    private LayoutInflater inflater=null;
    HashMap<String, Integer> alphaIndexer;
    String[] sections;
    private ImageProcessor mImageProcessor;
    private boolean delete;

    public AdpContacts(Activity a, ArrayList<ClsProfile> d,boolean _delete) {
    	delete = _delete;
        activity = a;
        data = d;if (d==null) return;
        if (activity==null) return;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        alphaIndexer = new HashMap<String, Integer>();
        mImageProcessor = new MaskImageProcessor(7);
        int size = d.size();

        for (int x = 0; x < size; x++) {
        	ClsProfile p = d.get(x);

	// get the first letter of the store
            String ch =  p.getInd_name();
	// convert to uppercase otherwise lowercase a -z will be sorted after upper A-Z
            ch = ch.toUpperCase();

	// HashMap will prevent duplicates
            alphaIndexer.put(ch, x);
        }

        Set<String> sectionLetters = alphaIndexer.keySet();

    // create a list from the set to sort
        ArrayList<String> sectionList = new ArrayList<String>(sectionLetters); 

        Collections.sort(sectionList);

        sections = new String[sectionList.size()];

        sectionList.toArray(sections);
    }

    public int getCount() {
        return data.size();
    }
    
    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
    	
        return position;
    }
    
    public static class ViewHolder{
        public TextView header,divider;
        public AsyncImageView img;
        public ImageView online,del;
    }
    


    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        final ViewHolder holder;
        
        if(convertView==null){
        	vi = inflater.inflate(R.layout.contacts_row, null);
            holder=new ViewHolder();
            holder.header=(TextView)vi.findViewById(R.id.header);
            holder.divider=(TextView)vi.findViewById(R.id.cnt_divider);
            holder.online = (ImageView)vi.findViewById(R.id.online);
            holder.del = (ImageView)vi.findViewById(R.id.delete);
            holder.img = (AsyncImageView)vi.findViewById(R.id.user_thumb);
            holder.img.setImageProcessor(mImageProcessor);
            vi.setTag(holder);
        }
        else
            holder=(ViewHolder) vi.getTag();
        
        ClsProfile o = data.get(position);
        if (o != null) {
        	
        	String tmp = o.getFirst_name()+ " "+ o.getLast_name();
        	if (tmp.equals("")) tmp = o.getScreen_name();
        	holder.header.setText(tmp);
        	if (o.getInd_name().equals("")) {
        		holder.divider.setVisibility(View.GONE);
        	}
        	else {
        		String lastInd = "";
        		if (position>0) {
        			ClsProfile oo = data.get(position-1);
        			lastInd = oo.getInd_name();
        		}
        		if (lastInd.equals(o.getInd_name())) {
        			holder.divider.setVisibility(View.GONE);
        		}
        		else {
        			holder.divider.setVisibility(View.VISIBLE);
        		}
        	}
        	holder.divider.setText(o.getInd_name());
        	//Log.d(TAG, ""+o.getPhoto());
        	if (!o.getPhoto().equals(""))
        		holder.img.setUrl(o.getPhoto());
        	else
        		holder.img.setImageResource(R.drawable.contact_nophoto);
        	if (o.getOnline()==1) {
        		holder.online.setVisibility(View.VISIBLE);
        	}
        	else {
        		holder.online.setVisibility(View.GONE);
        	}
        	if (delete) {
        		holder.del.setVisibility(View.VISIBLE);
        		holder.online.setVisibility(View.GONE);
        	}
        	else {
        		holder.del.setVisibility(View.GONE);
        	}
        	
        }
        return vi;
    }

	@Override
	public int getPositionForSection(int section) {
		// TODO Auto-generated method stub
		//return 0;
		return alphaIndexer.get(sections[section]);
	}

	@Override
	public int getSectionForPosition(int position) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public Object[] getSections() {
		// TODO Auto-generated method stub
		//return null;
		return sections;

	}
    
    

}

