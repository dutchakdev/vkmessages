package ikillingtime.com.ivkontaktepro;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 22.11.12
 * Time: 18:49
 * To change this template use File | Settings | File Templates.
 */
public class ClsAlbums {
    private String owner_id;
    private String album_id;
    private String title;

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
