package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.androidquery.AQuery;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 27.02.13
 * Time: 18:36
 * To change this template use File | Settings | File Templates.
 */
public class AdpComments extends BaseAdapter implements View.OnClickListener{

    Context context;
    ArrayList<JSONObject> data;
    LayoutInflater mInflater;
    protected AQuery listAq;
    Map<String,ClsUser> groupsMap;

    public AdpComments(Context context, ArrayList<JSONObject> data, HashMap<String, ClsUser> groupsMap) {
        this.context = context;
        this.data = data;
        this.groupsMap = groupsMap;
        mInflater = LayoutInflater.from(context);
        listAq = new AQuery(context);
    }
    @Override
    public void onClick(View view) {

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.comments_row, null);
        }

        JSONObject o = data.get(position);

        AQuery aq = listAq.recycle(convertView);
        //groups
        String groupName = o.optString("uid");
        if (groupsMap.containsKey(groupName)) {
            ClsUser user =groupsMap.get(groupName);
            groupName = user.getUsername();
            aq.id(R.id.user_thumb).image(user.getAvatar(),true,false);
        }
        aq.id(R.id.header).text(Html.fromHtml(groupName));

        aq.id(R.id.body).text(Html.fromHtml(o.optString("text", "")));

        aq.id(R.id.body).clicked(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView t = (TextView) view;

                t.setMaxLines(100);

            }
        });
        aq.id(R.id.time).text(Utils.convertTime(new Date( 1000l * o.optInt("date"))));

        return convertView;

    }

}
