package ikillingtime.com.ivkontaktepro;

import android.widget.*;
import com.androidquery.util.AQUtility;
import ikillingtime.com.ivkontaktepro.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.c2dm.C2DMessaging;

import utils.imageloader.AsyncImageView;
import utils.serviceloader.RESTService;
import utils.serviceloader.RESTService2;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

public class ActDialogs extends Fragment {
		private static final String TAG = "ActDialogs";
		private ArrayList<ClsDialog> dlgs,savedDlgs;
		private ResultReceiver mReceiver;
		private AdpDialogs adapter;
		private int dlgsSize;
		private String sig, longTs,longKey,longServer;
		private CmpDialogs mCmpDialogs;
		private CmpProfiles mCmpProfiles;
		private boolean paused = false,clearList = true,isRunning=false;
		private String metod;
		private Activity activity;
		private ListView list;
		private int offset;
		private ProgressBar progressBar;
        private LinearLayout searchPanel;
		
		
		@Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Log.d(TAG, "init");
            dlgs = new ArrayList<ClsDialog>();
            ArrayList<ClsProfile> profiles = new ArrayList<ClsProfile>();
            adapter = new AdpDialogs(getActivity(),dlgs);
            mCmpDialogs = new CmpDialogs();
            mCmpProfiles = new CmpProfiles();
            dlgsSize = 0;
            offset = 0;
            longTs = "";
            longKey = "";
            longServer = "";
            metod = "/method/execute?access_token="+AndroidApplication.getPrefsString("access_token");
            activity = getActivity();
            
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.dialogs, container, false);

            EditText searchTxt = (EditText) v.findViewById(R.id.dlg_searchtxt);
            progressBar = (ProgressBar)v.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
            list = (ListView) v.findViewById(R.id.list);
            Log.d(TAG, "createView");
            ImageView comp = (ImageView)v.findViewById(R.id.compose);
            comp.setOnClickListener(onComposeClk);
            
            list.setAdapter(adapter);
            list.setOnScrollListener(onScroll);
            list.setOnItemClickListener(onDlgClick);
            AnimationSet set = new AnimationSet(true);
            Animation animation = new AlphaAnimation(0.0f, 1.0f);
            animation.setDuration(400);
            set.addAnimation(animation);
            animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
            );
            animation.setDuration(400);
            set.addAnimation(animation);

            LayoutAnimationController controller =
                new LayoutAnimationController(set, 0.25f);
            list.setLayoutAnimation(controller);
            mReceiver = new ResultReceiver(new Handler()) {
                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                        //onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                        onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                    }
                    else {
                        onRESTResult(resultCode, null,0);
                    }
                }
                
            };

            searchPanel = (LinearLayout) v.findViewById(R.id.search_panel);


            final Button srchBtn = (Button) v.findViewById(R.id.search_icon);
            srchBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (searchPanel.getVisibility() == View.VISIBLE) {
                        searchPanel.startAnimation(new VisibleGoneAnimation(1.0f, 1.0f, 1.0f, 0.0f, 500, searchPanel, true));
                    }
                    else {
                        searchPanel.startAnimation(new VisibleGoneAnimation(1.0f, 1.0f, 0.0f, 1.0f, 500, searchPanel, false));
                    }
                }
            });
            searchTxt.setText("");
            
            //TODO ?? apply filters on fly or reset?
            searchTxt.addTextChangedListener(new TextWatcher(){
        	    public void afterTextChanged(Editable s){
        	    	Log.d(TAG, s.toString());
        	        filterByName(s.toString());
        	    }
        	    public void beforeTextChanged(CharSequence s, int start, int count, int after){
        	    	saveProfiles(s.toString());
        	    }
        	    public void onTextChanged(CharSequence s, int start, int before, int count){ }
        	});
            
            //C2DM??? ��� ������� ��������
            //String messageId = ""+activity.getIntent().getStringExtra("msg_id");
            	//Toast.makeText(activity, messageId, Toast.LENGTH_LONG).show();
            	//clearList = true;
				//offset = 0;
            	//onBrowse(false);
            
            return v;
        }
        

        private String restRequest(Bundle params,Boolean useCashe,int httpVerb,int mode,String method) {
        	isRunning = true;
        	progressBar.setVisibility(View.VISIBLE);

        	StringBuilder paramsString = new StringBuilder();
        	SortedMap<String, String> paramsMap = new TreeMap<String, String>();
        	for (String key : params.keySet()) {
        		paramsMap.put(key, params.get(key).toString());
        	}

        	for (String key : paramsMap.keySet()) {
                paramsString.append("&");
                paramsString.append(key);
                paramsString.append("=");
                paramsString.append(paramsMap.get(key).toString());
        	}
        	if (method.equals("")) method = "execute";
        	String methodString = "/method/"+method+"?access_token="+AndroidApplication.getPrefsString("access_token");
    		String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));
    		
    		if (useCashe) {
    			Log.w(TAG,methodString);
            	//parseResult(Utils.loadFromCashe(sig),useCashe);
            	//Collections.sort(dlgs, mCmpDialogs);
            	//this.sig = sig;
            }
    		
            Intent intent = new Intent(activity, RESTService.class);
            intent.setData(Uri.parse(AndroidApplication.HOST+methodString+"&sig="+sig));
            intent.putExtra(RESTService.EXTRA_HTTP_VERB, httpVerb);
            intent.putExtra(RESTService.EXTRA_MODE, mode);
            intent.putExtra(RESTService.EXTRA_PARAMS, params);
            intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
            activity.startService(intent);
            return sig;
    	}

        private void onRESTResult(int code, String result, int mode) {
        	if (paused == true) return;
        	progressBar.setVisibility(View.GONE);

            if (code == 200 && result != null) {
            	Log.d(TAG, "onRESTResult:"+result);
            	if (result.contains("error_msg")) {
     				try {
     					JSONObject o = new JSONObject(result);
     					o = o.optJSONObject("error");
     					Toast.makeText(activity, "Error:"+o.optString("error_msg"), Toast.LENGTH_LONG).show();
     				}
     				catch (Exception e) {}
     				return;
     			}
            	switch(mode) {
            	case 0:
            		parseResult(result,false);//��������
                	Collections.sort(dlgs, mCmpDialogs);//������
                	isRunning = false;
                	
                	offset+=50;
            		break;
            	case 1:
            		try {
    					JSONObject mJSONObject = new JSONObject(result);
    			    	mJSONObject = mJSONObject.optJSONObject("response");
    			    	mJSONObject = mJSONObject.optJSONObject("longpoll");
    			    	longKey = mJSONObject.optString("key");
    			    	longServer = Uri.decode(mJSONObject.optString("server"));
    			    	longTs = mJSONObject.optString("ts");
            		}
            		catch (Exception e) {}
            		if (!longKey.equals("") && !longServer.equals("") && !longTs.equals("")) {
            			getUpdates();
            			//TODO upd?
            			//clearList = true;
            	        //onBrowse(true);
            		}
            		break;
            	case 2:
            		JSONArray updates = null; 
            		try{
            			JSONObject mJSONObject = new JSONObject(result);
    			    	longTs = mJSONObject.optString("ts");
    			    	updates = mJSONObject.optJSONArray("updates");
            		}
            		catch (Exception e) {}
            		if (!longTs.equals("")) {
            			if (updates == null || updates.length()==0) {
            				getUpdates(); 
            			}
            			else {
            				//parse updates
            				parseUpdates(updates);
            				Collections.sort(dlgs, mCmpDialogs);
            			}
            		}
            		break;
            	}
            }
            else {

            }
        }
		
		private OnItemClickListener onDlgClick = new AdapterView.OnItemClickListener() 
	    {
			@Override
			public void onItemClick(AdapterView<?> av, View arg1,
					int position, long arg3) {
				AndroidApplication.setDlg(dlgs.get(position));
				startActivity(ActDialog.createIntent(getActivity()));
			}
	    };
	    
	    private void parseResult(String result,boolean useCashe) {
	    	Log.d(TAG, "parseresult");
	    	if (result==null) return;
	    	if (clearList) dlgs.clear();
        	JSONObject mJSONObject = null;
			ClsDialog d;
			ClsProfile p;
			//���������������,��������������� �����
			ArrayList<ClsProfile> profiles = new ArrayList<ClsProfile>();
			Map<String, Integer> profileMap = new HashMap<String, Integer>();
	    	try
	        {
	    		//{"dialogs":[11,{"mid":166,"date":1339577366,"out":1,"uid":97768291,"read_state":1,"title":" ... ","body":"3"},
	    		//{"mid":151,"date":1339269416,"out":0,"uid":97768291,"read_state":1,"title":"�������, �����","body":"Qwer","chat_id":1,"chat_active":"97768291,150882319","users_count":3,"admin_id":97768291},{"mid":117,"date":1339085408,"out":0,"uid":61745456,"read_state":0,"title":" ... ","body":"3"},{"mid":82,"date":1327799617,"out":0,"uid":12264856,"read_state":1,"title":"������ � ������","body":"�������  ����� ����� . �������� ���� ��� �2� ��������������� ����� �����������. ���� ���������"},{"mid":78,"date":1291732860,"out":1,"uid":98138800,"read_state":0,"title":"Re(3):  ...","body":"�� , ���� ������� ������� ��������� - ���� ���������� ���������� ��� ������ ������: http:\/\/leprastuff.ru\/chat\/<br>��� ������� ������ ���������� (�������������) ����������� ��������� �� ���� � �2� (��� �������)<br>�� ������ ��� ��������� ���� ��������� �� ���� ���������� � ��� ����� ���� ���<br>�� � �� ������ ���� ��� ���� ��� ���������"},{"mid":72,"date":1286082628,"out":1,"uid":86559892,"read_state":0,"title":"Re(3):  ...","body":"������"},{"mid":68,"date":1285853172,"out":1,"uid":14676209,"read_state":1,"title":"Re(2):  ...","body":"1000 ��������"},{"mid":65,"date":1285772608,"out":0,"uid":5591019,"read_state":1,"title":" ... ","body":"�������� �� ����� �� ���������� ?"},{"mid":64,"date":1285737924,"out":1,"uid":79821580,"read_state":1,"title":"Re(4): ����� ������?","body":"� ���� ��� �������, 1000 ���� �� ����"},{"mid":59,"date":1284565160,"out":1,"uid":60898616,"read_state":1,"title":"Re(3):  ...","body":"�����?"},{"mid":51,"date":1284487328,"out":1,"uid":1095283,"read_state":0,"title":"Re(2):  ...","body":"���, � ����� �� ���)"}],"profiles":[{"uid":97768291,"first_name":"Recoilme","last_name":"Recoilme","photo":"http:\/\/cs406619.userapi.com\/u97768291\/e_8e7cd4b8.jpg","online":1,"screen_name":"id97768291","mobile_phone":"","home_phone":""},{"uid":61745456,"first_name":"�������","last_name":"�������","photo":"http:\/\/cs9531.userapi.com\/u61745456\/e_916e7897.jpg","online":0,"screen_name":"id61745456","mobile_phone":"","home_phone":""},{"uid":12264856,"first_name":"������","last_name":"���������","photo":"http:\/\/cs10814.userapi.com\/u12264856\/e_d8e79a91.jpg","online":0,"screen_name":"dominicanoff","mobile_phone":"+79111256806 ","home_phone":"SKYPE: sat-internet"},{"uid":98138800,"first_name":"����","last_name":"�������","photo":"http:\/\/vk.com\/images\/deactivated_c.gif","online":0},{"uid":86559892,"first_name":"DELETED","last_name":"","photo":"http:\/\/vk.com\/images\/deactivated_c.gif","online":0},{"uid":14676209,"first_name":"�������","last_name":"��������","photo":"http:\/\/vk.com\/images\/deactivated_c.gif","online":0},{"uid":5591019,"first_name":"�������","last_name":"�����","photo":"http:\/\/cs11382.userapi.com\/u5591019\/e_ad230116.jpg","online":1,"screen_name":"ymalov"},{"uid":79821580,"first_name":"DELETED","last_name":"","photo":"http:\/\/vk.com\/images\/deactivated_c.gif","online":0},{"uid":60898616,"first_name":"�������","last_name":"��������","photo":"http:\/\/vk.com\/images\/deactivated_c.gif","online":0},{"uid":1095283,"first_name":"�������","last_name":"������","photo":"http:\/\/cs10989.userapi.com\/u1095283\/e_d83ba91e.jpg","online":0,"screen_name":"d.k.ritual","mobile_phone":"","home_phone":""}]}}

		    	mJSONObject = new JSONObject(result.toString());
		    	mJSONObject = mJSONObject.optJSONObject("response");
		    	JSONArray mDialogs = mJSONObject.optJSONArray("dialogs");
		    	JSONArray mProfiles = mJSONObject.optJSONArray("profiles");
		    	dlgsSize = mDialogs.optInt(0);
		    	
		    	if (mProfiles == null) return;
		    	for (int i=0;i<mProfiles.length();i++) {
		    		mJSONObject = mProfiles.getJSONObject(i);
		    		//"profiles":[{"uid":61745456,"first_name":"�������","mobile_phone":"","last_name":"�������","home_phone":"","screen_name":"id61745456","photo":"http:\/\/cs9531.userapi.com\/u61745456\/e_916e7897.jpg","online":0},{"uid":150882319,"first_name":"�����","mobile_phone":"790676500**","last_name":"����","home_phone":"671-30**","screen_name
		    		p = new ClsProfile();
		    		p.setUid(mJSONObject.optInt("uid"));
			    	p.setFirst_name(mJSONObject.optString("first_name"));
			    	p.setLast_name(mJSONObject.optString("last_name"));
			    	p.setScreen_name(mJSONObject.optString("screen_name"));
			    	p.setPhoto(Uri.decode(AndroidApplication.isLow()?mJSONObject.optString("photo"):mJSONObject.optString("photo_medium")));
			    	p.setOnline(mJSONObject.optInt("online"));
			    	profiles.add(p);
			    	profileMap.put(mJSONObject.optString("uid"), i);

		    	}
		    	
		    	if (dlgsSize==0) return;
		    	for (int i=1;i<mDialogs.length();i++) {
		    		mJSONObject = mDialogs.getJSONObject(i);
		    		d = new ClsDialog();
		    		//{"uid":61745456,"body":"��� ����","title":" ... ","read_state":0,"mid":42,"date":1338209468,"out":1},{"uid":150882319,"body":"��� �� ����?","title":" ... ","read_state":1,"mid":40,"date":1336281089,"out":0}
		    		
		    		d.setUid(mJSONObject.optInt("uid"));
		    		d.setProfile(profiles.get(profileMap.get(mJSONObject.optString("uid"))));
		    		d.setBody(mJSONObject.optString("body"));
		    		d.setTitle(mJSONObject.optString("title"));
		    		d.setRead_state(mJSONObject.optInt("read_state"));
		    		d.setMid(mJSONObject.optInt("mid"));
		    		d.setOut(mJSONObject.optInt("out"));
		    		d.setDate(new Date( 1000l * mJSONObject.getInt("date")));
		    		d.setChat_id(mJSONObject.optInt("chat_id"));
		    		d.setChat_active(mJSONObject.optString("chat_active"));
		    		d.setUsers_count(mJSONObject.optInt("users_count"));
		    		if (clearList) {
		    			//�������������� ��������
		    			dlgs.add(d);
		    		}
		    		else {
		    			//�������� ����� � ������� (��������� �������, ������ ����� - ����� ���������� � ������� �����������)
		    			int index = Collections.binarySearch(dlgs, d, mCmpDialogs);
		    			if (index>=0)
		    				dlgs.set(index, d);
		    			else
		    				dlgs.add(d);
		    		}
		    	}
	        }
	    	catch (Exception e)
	        {
	    		Log.e(TAG, e.toString());
	        }
	    	finally {
	    		mJSONObject = null;
	    	}
	    	adapter.notifyDataSetChanged();

	    	if (!useCashe) {
	    		//�������� ���
	    		Utils.save2cache(result,sig);
	    	}
	    }

        @Override
	    public void onResume() {
	        super.onResume();
	        paused = false;
	      //get longpoll
            /*
	        String code = "var messages = API.messages.getLongPollServer({});" +
	        		"return {longpoll: messages};";
	        String sig2 = Utils.md5(metod+"&code="+code+ AndroidApplication.getPrefsString("secret"));
	        Intent intent = new Intent(activity, RESTService2.class);
	        intent.setData(Uri.parse(AndroidApplication.HOST+metod+"&sig="+sig2));
	        Bundle params = new Bundle();
	        params.putString("code", code);
	        intent.putExtra(RESTService2.EXTRA_HTTP_VERB, 0x2);
	        intent.putExtra(RESTService2.EXTRA_MODE, 1);
	        intent.putExtra(RESTService2.EXTRA_PARAMS, params);
	        intent.putExtra(RESTService2.EXTRA_RESULT_RECEIVER, mReceiver);
	        activity.startService(intent);
             */
            clearList = true;
            offset = 0;
            onBrowse(true);

	    }

        @Override
	    public void onPause() {
	        super.onPause();
	        paused = true;
	    }
		
		private void getUpdates() {

	    	Intent intent = new Intent(activity, RESTService2.class);
	        intent.setData(Uri.parse("http://"+longServer+"?act=a_check&key="+longKey+"&ts="+longTs+"&wait=25&mode=2"));

	        intent.putExtra(RESTService2.EXTRA_HTTP_VERB, 0x2);
	        intent.putExtra(RESTService2.EXTRA_MODE, 2);
	        intent.putExtra(RESTService2.EXTRA_PARAMS, new Bundle());
	        intent.putExtra(RESTService2.EXTRA_RESULT_RECEIVER, mReceiver);
	        activity.startService(intent);
	    }
		
		private void parseUpdates(JSONArray updatesContainer) {
			//06-04 14:02:44.840: D/ActDialog(1228): [[4,48,35,61745456,1338818557," ... ","R",{}]]
			JSONArray updates = null;
			Log.d(TAG, "parseUpdates");
			try
	        {
				//4,$message_id,$flags,$from_id,$timestamp,$subject,$text,$attachments -- ���������� ������ ��������� 
				//[4,144,33,97768291,1339143358," ... ","Msg from rec",{}]

				for (int i=0;i<updatesContainer.length();i++) {
					updates = updatesContainer.getJSONArray(i);
					int mode = 0;
					mode = updates.getInt(0);
					switch(mode) {
					case 4:
						clearList = true;
						offset = 0;
	                	onBrowse(false);
		    			break;
					}
				}
				
	        }
			catch (Exception e) {
				Log.e(TAG, e.toString());
			}
			//update UI
			adapter.notifyDataSetChanged();
			//���������� - ��� �� ������ ���������� ������������
			getUpdates();
		}
		
		private OnScrollListener onScroll = new OnScrollListener() {

			@Override
	        public void onScrollStateChanged(AbsListView view,
					int scrollState) {
	        	searchAsyncImageViews(view, scrollState == OnScrollListener.SCROLL_STATE_FLING);
	        }
	        
	        private void searchAsyncImageViews(ViewGroup viewGroup, boolean pause) {
		        final int childCount = viewGroup.getChildCount();
		        for (int i = 0; i < childCount; i++) {
		            AsyncImageView image = (AsyncImageView) viewGroup.getChildAt(i).findViewById(R.id.user_thumb);
		            if (image != null) {
		                image.setPaused(pause);
		            }
		        }
		    }
	            
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (visibleItemCount > 0 && firstVisibleItem + visibleItemCount == totalItemCount &&
                		(offset>0) && !isRunning && offset<dlgsSize) {
                	Log.d(TAG,""+dlgsSize+":"+offset);
                	clearList = false;
                	onBrowse(false);
                }
            }
	    };
	    
	    private void onBrowse(boolean useCashe) {
	    	String code = "var dialogs = API.messages.getDialogs({count:50,offset:"+offset+"});" +
            		"var profiles = API.getProfiles({uids: dialogs@.uid,fields: \"photo,photo_medium,online,screen_name,contacts\"});" +
            		"return {dialogs: dialogs, profiles: profiles};";
        	Bundle params = new Bundle();
		    params.putString("code", code);
        	restRequest(params,useCashe,2,0,"");
	    }
	    
	    private void saveProfiles (String name) {
	    	if (name.trim().equals("")) {
	    		savedDlgs = dlgs;
	    	}
	    }
	    
	    private void filterByName (String name) {
	    	if (name.trim().equals("")) {
	    		dlgs = savedDlgs;
	    		adapter = new AdpDialogs(getActivity(),dlgs);
	            list.setAdapter(adapter);
		    	adapter.notifyDataSetChanged();
		    	return;
	    	}
	    	if (dlgs==null) return;
	    	if (dlgs.size()==0) return;
	    	ArrayList<ClsDialog> newDlgs = new ArrayList<ClsDialog>();
	    	for (ClsDialog d: dlgs) {
	    		if (d.getProfile()!=null) {
	    			ClsProfile p = d.getProfile();
	    			String fullName = p.getFirst_name().toLowerCase()+p.getLast_name().toLowerCase()+p.getScreen_name().toLowerCase();
		    		if (fullName.contains(name)) {
		    			newDlgs.add(d);
		    		}
	    		}
	    		
	    	}
	    	dlgs = newDlgs;
	    	adapter = new AdpDialogs(getActivity(),dlgs);
            list.setAdapter(adapter);
	    	adapter.notifyDataSetChanged();
	    }
	    
	    private OnClickListener onComposeClk = new View.OnClickListener() 
	    {
			@Override
			public void onClick(View v) {
				ActTabs.setContact();
			}
        	    };
}

