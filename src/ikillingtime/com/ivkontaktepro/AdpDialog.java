package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import utils.imageloader.AsyncImageView;
import utils.imageloader.ImageProcessor;
import utils.imageloader.MaskImageProcessor;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public class AdpDialog extends BaseAdapter {
    
	private static final String TAG = "AdpDialog";
    private Activity activity;
    private ArrayList<ClsDialog> data;
    private LayoutInflater inflater=null;
    private int uid;
    private ImageProcessor mImageProcessor;

    public AdpDialog(Activity a, ArrayList<ClsDialog> d) {
        activity = a;
        data = d;
        if (activity==null) return;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mImageProcessor = new MaskImageProcessor(7);
    }

    public AdpDialog(ActDialog a, ArrayList<ClsDialog> d, int uid) {
    	activity = a;
        data = d;
        this.uid = uid;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mImageProcessor = new MaskImageProcessor(7);
	}

	public int getCount() {
        return data.size();
    }
    
    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
    	
        return position;
    }
    
    public static class ViewHolder{
        public TextView in,out,intime,outtime;
        public RelativeLayout rowout,rowin;
        public LinearLayout llin,llout;
        public AsyncImageView imgin,imgout,geoin,geoout,videoin,videoout,imgav;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        final ViewHolder holder;
        
        if(convertView==null){
        	vi = inflater.inflate(R.layout.dialog_row, null);
            
        	holder=new ViewHolder();
            
            holder.in=(TextView)vi.findViewById(R.id.dlg_in);
            holder.intime=(TextView)vi.findViewById(R.id.dlg_intime);
            
            holder.out=(TextView)vi.findViewById(R.id.dlg_out);
            holder.outtime=(TextView)vi.findViewById(R.id.dlg_outtime);
            
            holder.rowin=(RelativeLayout)vi.findViewById(R.id.dlg_row_in);
            holder.rowout=(RelativeLayout)vi.findViewById(R.id.dlg_row_out);
            holder.llin=(LinearLayout)vi.findViewById(R.id.dlg_llin);
            holder.llout=(LinearLayout)vi.findViewById(R.id.dlg_llout);
            
            holder.imgin=(AsyncImageView)vi.findViewById(R.id.att_photoin);
            holder.imgout=(AsyncImageView)vi.findViewById(R.id.att_photoout);
            holder.geoin=(AsyncImageView)vi.findViewById(R.id.att_geoin);
            holder.geoout=(AsyncImageView)vi.findViewById(R.id.att_geoout);
            holder.videoin=(AsyncImageView)vi.findViewById(R.id.att_videoin);
            holder.videoout=(AsyncImageView)vi.findViewById(R.id.att_videoout);
            holder.imgav=(AsyncImageView)vi.findViewById(R.id.dlg_inavatar);
            holder.imgin.setImageProcessor(mImageProcessor);
            holder.imgout.setImageProcessor(mImageProcessor);
            holder.geoin.setImageProcessor(mImageProcessor);
            holder.geoout.setImageProcessor(mImageProcessor);
            holder.videoin.setImageProcessor(mImageProcessor);
            holder.videoout.setImageProcessor(mImageProcessor);
            holder.imgav.setImageProcessor(mImageProcessor);
            
            vi.setTag(holder);
        }
        else
            holder=(ViewHolder) vi.getTag();
        
        ClsDialog o = data.get(position);
        if (o != null) {
        	String body = o.getBody();
        	String time = Utils.convertTime(o.getDate());
        	String imgUrl = "";
        	String imgVideoUrl = "";
        	
        	String imgAva = "";
        	final String coords = o.getCoords();
        	//avatar
        	if (o.getProfile()!=null) {
        		ClsProfile profile = o.getProfile();
        		imgAva = profile.getPhoto();
        	}
        	//att photo
        	final ArrayList<ClsAttPhoto> photos = o.getAtt_photos();
        	if (photos!=null && photos.size()>0) {
        		ClsAttPhoto photo = photos.get(0);
        		imgUrl = photo.getSrc();
        		body+="<br/><b>"+activity.getResources().getString(R.string.dlg_photo);
        		if (photos.size()>1) {
        			body+=": (+"+(photos.size()-1)+"</b>)";
        		}
        	}
        	//att video
        	final ArrayList<ClsAttVideo> videos = o.getAtt_videos();
        	if (videos!=null && videos.size()>0) {
        		ClsAttVideo video = videos.get(0);
        		imgVideoUrl = video.getSrc();
        		body+="<br/><b>"+activity.getResources().getString(R.string.dlg_video);
        		if (videos.size()>1) {
        			body+=": (+"+(videos.size()-1)+"</b>)";
        		}
        	}
        	holder.imgin.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					AndroidApplication.setPhotos(photos);
					activity.startActivity(ActPhotos.createIntent(activity));
				}
        	});
        	holder.imgout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					AndroidApplication.setPhotos(photos);
					activity.startActivity(ActPhotos.createIntent(activity));
				}
        	});
        	holder.geoin.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try{
			    		String uri = "google.navigation:q="+coords;
			    		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
			    		activity.startActivity(i); 
					}
					catch (ActivityNotFoundException e) {
						Toast.makeText(activity, "Google Navigator not found", Toast.LENGTH_LONG).show();
					}
				}
        	});
        	holder.geoout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try{
			    		String uri = "google.navigation:q="+coords;
			    		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
			    		activity.startActivity(i); 
					}
					catch (ActivityNotFoundException e) {
						Toast.makeText(activity, "Google Navigator not found", Toast.LENGTH_LONG).show();
					}
				}
        	});
        	holder.videoin.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (videos!=null && videos.size()>0) {
		        		ClsAttVideo video = videos.get(0);
		        		//getSrc(video.getId());
		        		ActVideo.id = video.getId();
						activity.startActivity(ActVideo.createIntent(activity));
					}
				}
        	});
        	holder.videoout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (videos!=null && videos.size()>0) {
		        		ClsAttVideo video = videos.get(0);
		        		//getSrc(video.getId());
		        		ActVideo.id = video.getId();
						activity.startActivity(ActVideo.createIntent(activity));
					}
				}
        	});
        	if (o.getOut()==0){
        		holder.rowout.setVisibility(View.GONE);
        		holder.rowin.setVisibility(View.VISIBLE);
        		if (o.getGeo()!=null && !o.getGeo().equals("")) {
        			holder.geoin.setVisibility(View.VISIBLE);
        			holder.geoin.setUrl(o.getGeo());
        		}
        		else {
        			holder.geoin.setVisibility(View.GONE);
        		}
        		
        		if (imgUrl.equals("")) {
        			holder.imgin.setVisibility(View.GONE);
        		}
        		else {
        			holder.imgin.setVisibility(View.VISIBLE);
        			holder.imgin.setUrl(imgUrl);
        		}
        		
        		if (imgVideoUrl.equals("")) {
        			holder.videoin.setVisibility(View.GONE);
        		}
        		else {
        			holder.videoin.setVisibility(View.VISIBLE);
        			holder.videoin.setUrl(imgVideoUrl);
        		}
        		
        		if (imgAva.equals("")) {
        			holder.imgav.setVisibility(View.GONE);
        		}
        		else {
        			holder.imgav.setVisibility(View.VISIBLE);
        			holder.imgav.setUrl(imgAva);
        		}
        		holder.imgav.setOnClickListener(new OnClickListener() {

    				@Override
    				public void onClick(View v) {
    					//Toast.makeText(activity, "111", Toast.LENGTH_SHORT).show();
    				}
        		});
        		holder.in.setText(Html.fromHtml(body));
        		holder.intime.setText(time);
        		if (o.isSelected())
        			holder.llin.setBackgroundResource(R.drawable.msgin_selected);
        		else
        			holder.llin.setBackgroundResource(R.drawable.msgin);
        	}
        	else {
        		holder.rowout.setVisibility(View.VISIBLE);
        		holder.rowin.setVisibility(View.GONE);
        		
        		if (o.getGeo()!=null && !o.getGeo().equals("")) {
        			holder.geoout.setVisibility(View.VISIBLE);
        			holder.geoout.setUrl(o.getGeo());
        		}
        		else {
        			holder.geoout.setVisibility(View.GONE);
        		}
        		
        		if (imgUrl.equals("")) {
        			holder.imgout.setVisibility(View.GONE);
        		}
        		else {
        			holder.imgout.setVisibility(View.VISIBLE);
        			holder.imgout.setUrl(imgUrl);
        		}
        		if (imgVideoUrl.equals("")) {
        			holder.videoout.setVisibility(View.GONE);
        		}
        		else {
        			holder.videoout.setVisibility(View.VISIBLE);
        			holder.videoout.setUrl(imgVideoUrl);
        		}
        		holder.out.setText(Html.fromHtml(body));
        		holder.outtime.setText(time);
        		if (o.isSelected())
        			holder.llout.setBackgroundResource(R.drawable.msgout_selected);
        		else
        			holder.llout.setBackgroundResource(R.drawable.msgout);
        	}
        	
        }
        return vi;
    }
    public String getSrc(String src) {
    	Bundle params = new Bundle();
	    params.putString("videos", src);
    
    	StringBuilder paramsString = new StringBuilder();
    	SortedMap<String, String> paramsMap = new TreeMap<String, String>();
    	for (String key : params.keySet()) {
    		paramsMap.put(key, params.get(key).toString());
    	}

    	for (String key : paramsMap.keySet()) {
            paramsString.append("&");
            paramsString.append(key);
            paramsString.append("=");
            paramsString.append(paramsMap.get(key).toString());
    	}
    	String method = "video.get";
    	String methodString = "/method/"+method+"?access_token="+AndroidApplication.getPrefsString("access_token");
		String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));
		try {
	    	HttpPost request = new HttpPost();
	        request.setURI(new URI(AndroidApplication.HOST+methodString+"&sig="+sig));
	        HttpPost postRequest = (HttpPost) request;
	        
	        if (params != null) {
	            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(paramsToList(params),"UTF-8");
	            postRequest.setEntity(formEntity);
	        }

	        DefaultHttpClient client = new DefaultHttpClient();
	
	        HttpResponse response = client.execute(request);
	        
	        HttpEntity responseEntity = response.getEntity();
	        StatusLine responseStatus = response.getStatusLine();
	        int        statusCode     = responseStatus != null ? responseStatus.getStatusCode() : 0;
	        if (responseEntity != null) {
	        	//Server: {"response":{"server":{"upload_url":"http:\/\/cs304912.vk.com\/upload.php?act=profile&mid=97768291&hash=9b6a26cfd0643483c3a80e93374decea&rhash=221c60e598df170b5507a02c5ef7d99e&swfupload=1&m_aid=-6"}}}
	        	String result = EntityUtils.toString(responseEntity);
	        	Log.d("suc", result);
	        	//{"response":[1,{"vid":162812403,"owner_id":154965609,"title":"���-��-�������(111)","description":"","duration":143,"link":"video162812403","image":"http:\/\/cs12479.userapi.com\/u63381559\/video\/m_0ead1eb5.jpg","image_medium":"http:\/\/cs12479.userapi.com\/u63381559\/video\/l_cd39669d.jpg","date":1337857620,"views":1,
	        	//"files":{"mp4_240":"http:\/\/cs12479.userapi.com\/u63381559\/video\/5ef88a39a8.240.mp4","mp4_360":"http:\/\/cs12479.userapi.com\/u63381559\/video\/5ef88a39a8.360.mp4","mp4_480":"http:\/\/cs12479.userapi.com\/u63381559\/video\/5ef88a39a8.480.mp4"},"player":"http:\/\/vk.com\/video_ext.php?oid=154965609&id=162812403&hash=dc730fa72c224db4"}]}
	        	
	        	return result;
	        }
		}
        catch (Exception e) {
        	Log.e("Video", e.toString());
        }
		return null;
    }
    private static List<BasicNameValuePair> paramsToList(Bundle params) {
        ArrayList<BasicNameValuePair> formList = new ArrayList<BasicNameValuePair>(params.size());
        SortedMap<String, String> paramsMap = new TreeMap<String, String>();
    	for (String key : params.keySet()) {
    		paramsMap.put(key, params.get(key).toString());
    	}

    	for (String key : paramsMap.keySet()) {
    		formList.add(new BasicNameValuePair(key, params.get(key).toString()));
    	}
        
        return formList;
    }
}

