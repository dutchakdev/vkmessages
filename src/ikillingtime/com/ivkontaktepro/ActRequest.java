package ikillingtime.com.ivkontaktepro;

import ikillingtime.com.ivkontaktepro.R;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.c2dm.C2DMessaging;

import utils.imageloader.AsyncImageView;
import utils.imageloader.MaskImageProcessor;
import utils.serviceloader.RESTService;
import utils.serviceloader.RESTService2;
import widget.ScrollingTextView;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ActRequest extends Activity {
	private static final String TAG = "ActRequest";
	public static String name="",phone="",url="";
	public static int uid=0;
	private ResultReceiver mReceiver;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request);
        
        final ScrollingTextView title = (ScrollingTextView)findViewById(R.id.pfl_title);
        title.setText(name);
        
        final AsyncImageView img =(AsyncImageView)findViewById(R.id.user_thumb);
        if (url.equals("")) {
        	img.setImageResource(R.drawable.contact_nophoto);
        }
        else {
        	img.setUrl(url);
        	img.setImageProcessor(new MaskImageProcessor(7));
        }
        mReceiver = new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                    //onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                    onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                }
                else {
                    onRESTResult(resultCode, null,0);
                }
            }
            
       };

    }
	
	public void onRESTResult(int code, String result, int mode) {
		Log.d(TAG, "onRESTResult:"+result);
		if (code == 200 && result != null) {
        	
			if (result.contains("error_msg")) {
				try {
					JSONObject o = new JSONObject(result);
					o = o.optJSONObject("error");
					Toast.makeText(this, "Error:"+o.optString("error_msg"), Toast.LENGTH_LONG).show();
				}
				catch (Exception e) {}
				return;
			}
		}

	}
	public static Intent createIntent(Context context) {
        Intent i = new Intent(context, ActRequest.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }
	public void onBack(View v) {
		finish();
	}
	public void onAdd(View v) {
		String code="var profiles = API.friends.add({uid:"+uid+"});" +
        		"return {profiles: profiles};";
        restRequest(code,0);
		finish();
	}
	public void onDecline(View v) {
		String code="var profiles = API.friends.delete({uid:"+uid+"});" +
        		"return {profiles: profiles};";
        restRequest(code,0);
		finish();
	}
	private String restRequest(String code,int mode) {
    	Log.d(TAG, code);
    	String metod = "/method/execute?access_token="+AndroidApplication.getPrefsString("access_token");
		String sig = Utils.md5(metod+"&code="+code+ AndroidApplication.getPrefsString("secret"));       
        Intent intent = new Intent(this, RESTService.class);
        intent.setData(Uri.parse(AndroidApplication.HOST+metod+"&sig="+sig));
        Bundle params = new Bundle();
        params.putString("code", code);
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, 0x2);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);
        intent.putExtra(RESTService.EXTRA_MODE, mode);
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
        startService(intent);
        return sig;
	}

	public void onSend(View v) {
		startActivity(ActDialog.createIntent(this));
	}


}


