package ikillingtime.com.ivkontaktepro;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 10.06.13
 * Time: 10:58
 * To change this template use File | Settings | File Templates.
 */
public class ClsMusic {

    private String aid;
    private String owner_id;// — идентификатор владельца аудиозаписи;
    private String artist;// — исполнитель;
    private String title;// — название композиции;
    private int duration;// — длительность аудиозаписи в секундах;
    private String durationStr;
    private String url;// — ссылка на mp3;
    private String lyrics_id;// — идентификатор текста аудиозаписи (если доступно);
    private String album;// — идентификатор альбома, в котором находится аудиозапись (если присвоен).
    private String folder;
    private String filename;
    private long id;

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLyrics_id() {
        return lyrics_id;
    }

    public void setLyrics_id(String lyrics_id) {
        this.lyrics_id = lyrics_id;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getDurationStr() {
        return durationStr;
    }

    public void setDurationStr(String durationStr) {
        this.durationStr = durationStr;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
