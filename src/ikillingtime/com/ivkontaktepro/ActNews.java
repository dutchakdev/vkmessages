package ikillingtime.com.ivkontaktepro;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;

import com.androidquery.util.AQUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 09.06.13
 * Time: 20:30
 * To change this template use File | Settings | File Templates.
 */
public class ActNews extends ActAbstractGroup {

    private Map<String,String> groupsMap;

    @Override
    public void initAdapter() {

        dlgs = new ArrayList<JSONObject>();
        groupsMap = new HashMap<String,String>();
        adapter = new AdpNews(activity,dlgs,groupsMap);
        my_string = getString(R.string.tab_news);
        customSpinner = 2;
    }
    public static void alert(Context context,String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(context);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);

        bld.create().show();
    }

    @Override
    public void listListeners() {
        Resources r = getResources();
        final float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, r.getDisplayMetrics());
        list.setColumnWidth((int)px);
        //list.setMinimumHeight((int)px*4);
        list.setOnScrollListener(onScroll);
        list.setOnItemClickListener(onListClick);
        playerControls.setVisibility(View.GONE);
        dialogs.setBackgroundDrawable(r.getDrawable(R.drawable.darkbackground));
    }

    private AbsListView.OnScrollListener onScroll = new AbsListView.OnScrollListener() {

        @Override
        public void onScrollStateChanged(AbsListView view,
                                         int scrollState) {
        }
        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (visibleItemCount > 0 && firstVisibleItem + visibleItemCount == totalItemCount &&
                     !isRunning && offset<dlgsSize) {

                clearList = false;
                isRunning = true;
                ((ActAbstractGroup) ActNews.this).increaseOffset();
                onBrowse();
            }
        }
    };

    private AdapterView.OnItemClickListener onListClick = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> av, View arg1,
                                int position, long arg3) {
            if (dlgs==null || dlgs.size()<=position)
                return;

        }
    };

    @Override
    public void getAlbums() {
        /*
        Bundle params = new Bundle();
        params.putString("count", ""+COUNT_MEDIUM);
        if (getGid()!=null && !getGid().equals("")) {
            params.putString("gid", ""+getGid());
        }
        request(params,1, "video.getAlbums");
        */
        ArrayList<ClsAlbums> albums = new  ArrayList<ClsAlbums>();
        ClsAlbums album = new ClsAlbums();
        album.setAlbum_id(""+R.string.tab_con);
        album.setOwner_id("");
        album.setTitle(getString(R.string.tab_con));
        albums.add(album);
        album = new ClsAlbums();
        album.setAlbum_id(""+R.string.dlg_photos);
        album.setOwner_id("");
        album.setTitle(getString(R.string.dlg_photos));
        albums.add(album);
        setAlbums(albums);
        String[] titles = new String[albums.size()];
        int i = 0;
        for (ClsAlbums a: albums) {
            titles[i] = a.getTitle();

            i++;
        }

        setupSpinnerAlbum(titles);
    }

    @Override
    public void getSdCartMusic() {

    }

    @Override
    public void parseResult(String result) {
        if (result==null) return;
        if (clearList) dlgs.clear();
        JSONObject mJSONObject = null;

        AQUtility.debug(result);
        JSONObject json = null;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        JSONObject o = json.optJSONObject("response");
        JSONArray ja = null;

        if (o==null) {
            ja = json.optJSONArray("response");
        }
        else {
            ja = o.optJSONArray("items");
        }

        if(ja == null) {
            return;
        }

        adapter.notifyDataSetInvalidated();
        if (clearList) {
            dlgs.clear();
        }
        dlgsSize = ja.optInt(0);
        int first = 1;
        if (dlgsSize==0) {
            dlgsSize = 300;
            first = 0;
        }
        for(int i = first; i < ja.length(); i++){
            JSONObject jo = ja.optJSONObject(i);
            dlgs.add(jo);
        }

        JSONArray groups = o.optJSONArray("groups");

        if (groups!=null) {
            for(int j=0;j<groups.length();j++) {
                JSONObject g = groups.optJSONObject(j);
                groupsMap.put("-"+g.optString("gid"),g.optString("name"));
            }
        }
        //profiles":[{"uid":29652,"first_name":"��","sex":2,"last_name":"�����"
        JSONArray profiles = o.optJSONArray("profiles");

        if (profiles!=null) {
            for(int j=0;j<profiles.length();j++) {
                JSONObject p = profiles.optJSONObject(j);
                groupsMap.put(p.optString("uid"),p.optString("first_name")+" "+p.optString("last_name"));
            }
        }

        adapter.notifyDataSetChanged();
        isRunning = false;
        if (getCacheTime()==0){
            setCacheTime(AndroidApplication.cacheTime);
            if (getCacheTime()>0) {
                firstRequest();
            }
        }

    }

    @Override
    public void processCustomAlbumSpinner(AdapterView<?> adpView,int pos) {

    }


    @Override
    public void onBrowse() {

        Bundle params = new Bundle();
        AndroidApplication.putPrefsString("CURNEWSGID",(getCurrentMode()==MODE_SEARCH)?"search:"+searchQuery.trim():getGid());
        switch (getCurrentMode()) {

            case MODE_GETALL:
                params.putString("count", ""+COUNT_MEDIUM);
                params.putString("filters", "post");
                params.putString("offset", ""+offset);
                request(params, 1, "newsfeed.get");
                break;
            case MODE_SEARCH:

                params.putString("count", ""+COUNT_MEDIUM);
                params.putString("offset", ""+offset);
                params.putString("q", searchQuery.trim());
                request(params, -1, "newsfeed.search");
                break;

            case MODE_GETALBUM_CONTENT:

                params.putString("count", ""+COUNT_MEDIUM);
                params.putString("max_photos", "100");
                if (aid!=null && aid.equals(""+R.string.dlg_photos)) {
                    params.putString("filters", "wall_photo");
                }
                else {
                    params.putString("filters", "post");
                }
                if (!getGid().equals("")) {
                    params.putString("source_ids",getGid().contains("u")?getGid().replace("u",""):"g"+getGid());
                    //""+AndroidApplication.getPrefsInt("user_id",0));
                    //"g"+getGid());
                }
                params.putString("offset", ""+offset);
                request(params, 1, "newsfeed.get");

                break;
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
