package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.serviceloader.RESTService;
import widget.ScrollingTextView;

import java.util.*;

public class ActDialogEdit extends FragmentActivity {
	private static final String TAG = "ActDialogEdit";
	private ClsDialog dlg;
	private ResultReceiver mReceiver;
	private Activity activity;
	private AdpContacts adapter;
	private ArrayList<ClsProfile> profiles;
	private ListView list;
	private EditText txt;
	private ScrollingTextView title;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_multi_edit);
        
        
        txt = (EditText) findViewById(R.id.dlg_multitxt);
        title = (ScrollingTextView) findViewById(R.id.dlg_title);
        String tmp = getString(R.string.dlg_members);
        dlg = AndroidApplication.getDlg();
        tmp = tmp.replace("{num}", ""+dlg.getUsers_count());
        title.setText(tmp);
        txt.setText(dlg.getTitle());

        activity = this;
        mReceiver = new ResultReceiver(new Handler()) {

            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey("REST_RESULT")) {
                    onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                }
                else {
                    onRESTResult(resultCode, null, 0);
                }
            }
            
        };
        Log.d(TAG, "init"+dlg.getChat_active());
        profiles = new ArrayList<ClsProfile>();
        adapter = new AdpContacts(activity,profiles,true);
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(onContactClick);
        if (AndroidApplication.getChooseContactId()==0) {
        	getContacts();
        }
        else {
        	Bundle params = new Bundle();
    	    params.putString("chat_id", ""+dlg.getChat_id());
    	    params.putString("uid", ""+AndroidApplication.getChooseContactId());
    	    AndroidApplication.setChooseContactId(0);
    		restRequest(params,2,2,"messages.addChatUser");
        }
    }
	
	private void getContacts() {
		String code = 
				"var profiles = API.messages.getChatUsers({chat_id:"+dlg.getChat_id()+",fields: \"photo,online,screen_name,contacts\"});"+
	    		//"var profile = API.getProfiles({uids: user@.uid,fields: \"photo,online,screen_name,contacts\"});" +
        		"return {profiles: profiles};";
		//String code = 
				//"var user = API.messages.getChat({chat_id:"+dlg.getChat_id()+"});"+
	    		//"var profiles = API.getProfiles({uids: \""+dlg.getChat_active()+","+AndroidApplication.getPrefsInt("user_id")+"\",fields: \"photo,online,screen_name,contacts\"});" +
        		//"return {profiles: profiles};";
        Bundle paramss = new Bundle();
	    paramss.putString("code", code);
		restRequest(paramss,2,0,"");
	}
	
    private String restRequest(Bundle params,int httpVerb,int mode,String method) {

    	StringBuilder paramsString = new StringBuilder();
    	SortedMap<String, String> paramsMap = new TreeMap<String, String>();
    	for (String key : params.keySet()) {
    		paramsMap.put(key, params.get(key).toString());
    	}

    	for (String key : paramsMap.keySet()) {
            paramsString.append("&");
            paramsString.append(key);
            paramsString.append("=");
            paramsString.append(paramsMap.get(key).toString());
    	}
    	if (method.equals("")) method = "execute";
    	String methodString = "/method/"+method+"?access_token="+AndroidApplication.getPrefsString("access_token");
		String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));

		Log.d(TAG, methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));
        Intent intent = new Intent(activity, RESTService.class);
        intent.setData(Uri.parse(AndroidApplication.HOST+methodString+"&sig="+sig));
        intent.putExtra(RESTService.EXTRA_MODE, mode);
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, httpVerb);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
        activity.startService(intent);
        return sig;
	}
    
	public void onRESTResult(int code, String result, int mode) {
        Log.d(TAG, "onRESTResult mode:"+mode+" res: "+result);
        // Check to see if we got an HTTP 200 code and have some data.
        if (code == 200 && result != null) {
        	//check captcha
        	if (result.contains("error_code\":14")) {
        		//parseCaptcha(result);
        		Intent intent = new Intent(this, ActCaptcha.class);
        		intent.putExtra("result", result);
		        //startActivityForResult(intent, CAPTCHA_RESULT);
		        return;
        	}
        	if (result.contains("error_msg")) {
 				try {
 					JSONObject o = new JSONObject(result);
 					o = o.optJSONObject("error");
 					Toast.makeText(activity, "Error:"+o.optString("error_msg"), Toast.LENGTH_LONG).show();
 				}
 				catch (Exception e) {}
 				return;
 			}
        	switch(mode) {
        	case 0:
        		parseResult(result,true);
        		break;
        	case 1:
        		if (result.equals("{\"response\":1}")) {
        			Toast.makeText(this, R.string.dlg_change_ok, Toast.LENGTH_LONG).show();
        			dlg.setTitle(txt.getText().toString());
        			AndroidApplication.setDlg(dlg);
        		}
        		break;
        	case 2:
        		getContacts();
        		break;
        	}
        }
        else {
        }
    }
	
	private void parseResult(String result,boolean firstRun) {
    	

    	profiles = new ArrayList<ClsProfile>();
        
    	if (result==null) return;

    	JSONObject mJSONObject = null;

		ClsProfile p;
    	try
        {
	    	mJSONObject = new JSONObject(result.toString());
	    	mJSONObject = mJSONObject.optJSONObject("response");
	    	JSONArray mProfiles = mJSONObject.optJSONArray("profiles");
	    	
	    	if (mProfiles == null) return;
	    	for (int i=0;i<mProfiles.length();i++) {
	    		mJSONObject = mProfiles.getJSONObject(i);

	    		p = new ClsProfile();
	    		if (i<=4) p.setInd_name("");
	    		else {
	    			if (mJSONObject.optString("first_name").length()>0)
	    				p.setInd_name(mJSONObject.optString("first_name").substring(0, 1));
	    			else p.setInd_name("");
	    		}
		    	p.setFirst_name(mJSONObject.optString("first_name"));
		    	p.setLast_name(mJSONObject.optString("last_name"));
		    	p.setScreen_name(mJSONObject.optString("screen_name"));
		    	p.setPhoto(Uri.decode(mJSONObject.optString("photo")));
		    	p.setOnline(mJSONObject.optInt("online"));
		    	p.setUid(mJSONObject.optInt("uid"));
		    	profiles.add(p);
	    		
	    	}
        }
    	catch (Exception e)
        {
    		Log.e(TAG, e.toString());
        }
    	finally {
    		mJSONObject = null;
    	}
    	Log.d(TAG, "size"+profiles.size());
    	String tmp = getString(R.string.dlg_members);
        tmp = tmp.replace("{num}", ""+profiles.size());
        title.setText(tmp);
        ClsDialog dd = AndroidApplication.getDlg();
		if (dd.getUsers_count()!=profiles.size()) {
			dd.setUsers_count(profiles.size());
			AndroidApplication.setDlg(dd);
		}
    	Collections.sort(profiles, new Comparator<ClsProfile>() {
            @Override
            public int compare(ClsProfile p1, ClsProfile p2) {
            	String s1 = p1.getInd_name();
            	String s2 = p2.getInd_name();
                return s1.compareToIgnoreCase(s2);
            }
        });

    	adapter = new AdpContacts(activity,profiles,true);
        list.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	if (!firstRun) {
    		//�������� ���
    		//Utils.save2cache(result,sig);
    	}
    }
	
	public static Intent createIntent(Context context) {
        Intent i = new Intent(context, ActDialogEdit.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }
	
	public void onBack(View v) {
		finish();
	}
	
	public void onEdit(View v) {
		String msg = txt.getText().toString().trim();
		if (txt.equals("")) {
			Animation shake = AnimationUtils.loadAnimation(ActDialogEdit.this, R.anim.shake);
			txt.startAnimation(shake);
			return;
		}
		Bundle params = new Bundle();
	    params.putString("chat_id", ""+dlg.getChat_id());
	    params.putString("title", msg);
		restRequest(params,2,1,"messages.editChat");
	}
	
	public void onNew(View v) {
		//Use fragments Luke!
		AndroidApplication.setChooseContact(1);
		FrameLayout frame = new FrameLayout(this);
        frame.setId(10101010);
        setContentView(frame, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

        //if (savedInstanceState == null) {
            Fragment newFragment = new ActContacts();
            
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(10101010, newFragment).commit();
        //}

	}
	private OnItemClickListener onContactClick = new AdapterView.OnItemClickListener() 
    {
		@Override
		public void onItemClick(AdapterView<?> av, View arg1,
				int position, long arg3) {
			ClsProfile p = profiles.get(position);
			Bundle params = new Bundle();
    	    params.putString("chat_id", ""+dlg.getChat_id());
    	    params.putString("uid", ""+p.getUid());
    		restRequest(params,2,2,"messages.removeChatUser");
		}
    };

}


