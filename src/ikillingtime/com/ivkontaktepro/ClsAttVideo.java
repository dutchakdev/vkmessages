package ikillingtime.com.ivkontaktepro;

public class ClsAttVideo {

	private String id;
	private String pid;
	private String owner_id;
	private String src;
	private String src_big;
	private String title;
	private String key;
	
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getOwner_id() {
		return owner_id;
	}
	public void setOwner_id(String owner_id) {
		this.owner_id = owner_id;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public String getSrc_big() {
		return src_big;
	}
	public void setSrc_big(String src_big) {
		this.src_big = src_big;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
