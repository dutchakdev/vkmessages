package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 27.02.13
 * Time: 18:36
 * To change this template use File | Settings | File Templates.
 */
public class AdpNews extends BaseAdapter {

    private static final String TAG = "AdpNews";
    Context context;
    ArrayList<JSONObject> data;
    LayoutInflater mInflater;
    protected AQuery listAq;
    Map<String,String> groupsMap;

    public AdpNews (Context context, ArrayList<JSONObject> data, Map<String,String> groupsMap) {
        this.context = context;
        this.data = data;
        this.groupsMap = groupsMap;
        mInflater = LayoutInflater.from(context);
        listAq = new AQuery(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.news_row, null);
        }

        final JSONObject o = data.get(position);

        final AQuery aq = listAq.recycle(convertView);
        //groups
        //AQUtility.debug(ActNewsOld.gid,":"+o.optString("source_id"));
        if (("-"+ ActNewsOld.gid).equals(o.optString("source_id"))) {
            aq.id(R.id.title).gone();
        }
        else {
            aq.id(R.id.title).visible();
            String groupName = o.optString("source_id");
            if (groupsMap.containsKey(groupName)) {
                groupName = groupsMap.get(groupName);
            }
            aq.id(R.id.title).text(Html.fromHtml(groupName));

        }
        aq.id(R.id.text).text(Html.fromHtml(o.optString("text", "")));

        aq.id(R.id.text).clicked(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                TextView t = (TextView) view;

                t.setMaxLines(100);

            }
        });
        //menu
        //info block
        JSONObject comm = o.optJSONObject("comments");
        String commCnt ="";
        if (comm != null) {
           commCnt = comm.optString("count");
           aq.id(R.id.icon_comm).visible();
        }
        else {
            aq.id(R.id.icon_comm).gone();
        }
        aq.id(R.id.comm_cnt).text(commCnt);

        aq.id(R.id.icon_comm).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //AQUtility.debug(TAG,items.get(i).toString());
                AndroidApplication.putPrefsString("post",o.toString());
                context.startActivity(ActComments.createIntent(context));
            }
        });

        //like
        //likes: {
        //count: 0,
        //       user_likes: 0,
        //       can_like: 1,
        //       can_publish: 1
        //},
        int user_likes =0;
        String likesCnt ="";
        JSONObject likes = o.optJSONObject("likes");
        if (likes!=null){
            user_likes = likes.optInt("user_likes",0);
            likesCnt = likes.optString("count");
            aq.id(R.id.icon_like).visible();
        }
        else {
            aq.id(R.id.icon_like).gone();
        }
        final ImageView imageView = ((ImageView)aq.id(R.id.icon_like).getView());
        if (user_likes==0) {
            imageView.setImageResource(R.drawable.btn_star);
        }
        else {
            imageView.setImageResource(R.drawable.btn_staractive);
        }

        final int userLikes = user_likes;
        aq.id(R.id.icon_like).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle params = new Bundle();
                params.putString("type", "post");
                params.putString("owner_id", o.optString("source_id"));
                params.putString("item_id", o.optString("post_id"));
                String method = "likes.add";
                if (userLikes==0) {
                    method = "likes.add";
                    imageView.setImageResource(R.drawable.btn_staractive);
                    Toast.makeText(context,R.string.like_added,Toast.LENGTH_SHORT).show();
                }
                else {
                    method = "likes.delete";
                    imageView.setImageResource(R.drawable.btn_star);
                    Toast.makeText(context,R.string.like_del,Toast.LENGTH_SHORT).show();
                }
                listAq.ajax(ActNewsOld.generateUrl(params, method),JSONObject.class,1,context,"");
            }
        });


        aq.id(R.id.likes_cnt).text(likesCnt);

        Date timestamp = new Date( 1000l * o.optInt("date"));
        aq.id(R.id.post_time).text(Utils.convertTime(timestamp ));

        //image
        final ArrayList<ClsAttPhoto> photosGalery = new ArrayList<ClsAttPhoto>();
        //aq.id(R.id.img).gone();

        if (o.optString("type").equals("wall_photo") || o.optString("type").equals("photo")) {
            JSONArray photos = o.optJSONArray("photos");
            if (photos!=null || photos.length()>0) {
                photosGalery.clear();
                for(int k=1;k<photos.length();k++) {
                    JSONObject photo = photos.optJSONObject(k);
                    ClsAttPhoto p = new ClsAttPhoto();
                    p.setId(photo.optString("owner_id")+"_"+photo.optString("pid"));
                    p.setSrc(Uri.decode(photo.optString("src")));
                    p.setSrc_big(Uri.decode(photo.optString("src_big")));

                    photosGalery.add(p);
                }
            }
        }
        else {
            if (o.has("attachments")) {
                JSONArray atts = o.optJSONArray("attachments");
                for(int k=0;k<atts.length();k++) {
                    JSONObject att = atts.optJSONObject(k);
                    if (att.optString("type").equals("photo")) {
                        att = att.optJSONObject("photo");
                        ClsAttPhoto photo = new ClsAttPhoto();
                        photo.setId(att.optString("owner_id")+"_"+att.optString("pid"));
                        photo.setSrc(Uri.decode(att.optString("src")));
                        photo.setSrc_big(Uri.decode(att.optString("src_big")));
                        photosGalery.add(photo);
                        //break;
                    }
                }
            }
        }
        //aq.id(R.id.img).visible();
        if (photosGalery.size()>0) {
            //aq.id(R.id.img).visible();
           // setImg(photosGalery ,aq.id(R.id.img),aq.id(R.id.img_cnt));
            final String src = photosGalery.get(0).getSrc_big();
            if (photosGalery.size()>1) {
                aq.id(R.id.img_cnt).text(context.getString(R.string.dlg_phototitle).replace("{1}", ""+(1)).replace("{n}", ""+photosGalery.size()));
            }
            else {
                aq.id(R.id.img_cnt).text("");
            }
            aq.id(R.id.img).image(src, true, false, 640, 0, null, AQuery.FADE_IN_NETWORK, AQuery.RATIO_PRESERVE);
            aq.id(R.id.img).clicked(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, ActPhotos.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    AndroidApplication.setPhotos(photosGalery);
                    context.startActivity(i);
                }
            });
        }
        else {
            aq.id(R.id.img).image("");
            aq.id(R.id.img_cnt).text("");
        }

        //end image

        //video
        aq.id(R.id.video_lout).gone();
        if (o.has("attachments")) {
            JSONArray atts = o.optJSONArray("attachments");
            for(int k=0;k<atts.length();k++) {
                JSONObject att = atts.optJSONObject(k);
                if (att.optString("type").equals("video")) {
                    att = att.optJSONObject("video");
                    final String videoId = att.optString("owner_id")+"_"+att.optString("vid");
                    aq.id(R.id.video_lout).visible();
                    int ddd = att.optInt("duration");
                    int seconds = (int) (ddd) % 60 ;
                    int minutes = (int) ((ddd / 60) % 60);
                    int hours   = (int) ((ddd / (60*60)) % 24);
                    String dur= ""+hours+":"+(minutes<10?"0"+minutes:minutes)+":"+(seconds<10?"0"+seconds:seconds);
                    aq.id(R.id.dur).text(dur);
                    aq.id(R.id.splash).image(att.optString("image_big"), true, false, 0, 0, null, AQuery.FADE_IN_NETWORK, AQuery.RATIO_PRESERVE);
                    aq.id(R.id.splash).clicked(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActVideo.id = videoId;
                            context.startActivity(ActVideo.createIntent(context));
                        }
                    });
                    break;
                }
            }
        }
        //end video

        return convertView;

    }
    void setImg(final ArrayList<ClsAttPhoto> photosGalery,AQuery aq, AQuery txt) {
        final String src = photosGalery.get(0).getSrc_big();
        if (photosGalery.size()>1) {
            txt.text(context.getString(R.string.dlg_phototitle).replace("{1}", ""+(1)).replace("{n}", ""+photosGalery.size()));
        }
        else {
            txt.text("");
        }
        aq.image(src, true, false, 640, 0, null, AQuery.FADE_IN_NETWORK, AQuery.RATIO_PRESERVE);
        aq.clicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ActPhotos.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                AndroidApplication.setPhotos(photosGalery);
                context.startActivity(i);
            }
        });
    }

}
