package ikillingtime.com.ivkontaktepro;

import java.util.ArrayList;
import java.util.Date;

public class ClsDialog {
	private int uid;
	private String body;
	private String title;
	private int read_state;
	private int mid;
	private Date date;
	private int out;
	private ClsProfile profile;
	private int chat_id;
	private String chat_active;
	private int users_count;
	private boolean selected;
	private ArrayList<ClsAttPhoto> att_photos;
	private ArrayList<ClsAttVideo> att_videos;
	private String geo;
	private String coords;

	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getRead_state() {
		return read_state;
	}
	public void setRead_state(int read_state) {
		this.read_state = read_state;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getOut() {
		return out;
	}
	public void setOut(int out) {
		this.out = out;
	}
	public ClsProfile getProfile() {
		return profile;
	}
	public void setProfile(ClsProfile profile) {
		this.profile = profile;
	}
	public int getChat_id() {
		return chat_id;
	}
	public void setChat_id(int chat_id) {
		this.chat_id = chat_id;
	}
	public String getChat_active() {
		return chat_active;
	}
	public void setChat_active(String chat_active) {
		this.chat_active = chat_active;
	}
	public int getUsers_count() {
		return users_count;
	}
	public void setUsers_count(int users_count) {
		this.users_count = users_count;
	}
	public ArrayList<ClsAttPhoto> getAtt_photos() {
		return att_photos;
	}
	public void setAtt_photos(ArrayList<ClsAttPhoto> att_photos) {
		this.att_photos = att_photos;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public String getGeo() {
		return geo;
	}
	public void setGeo(String geo) {
		this.geo = geo;
	}
	public String getCoords() {
		return coords;
	}
	public void setCoords(String coords) {
		this.coords = coords;
	}
	public ArrayList<ClsAttVideo> getAtt_videos() {
		return att_videos;
	}
	public void setAtt_videos(ArrayList<ClsAttVideo> att_videos) {
		this.att_videos = att_videos;
	}

}
