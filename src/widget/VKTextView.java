package widget;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * @author marco
 * Workaround to be able to scroll text inside a TextView without it required
 * to be focused. For some strange reason there isn't an easy way to do this
 * natively.
 * 
 * Original code written by Evan Cummings:
 * http://androidbears.stellarpc.net/?p=185
 */
public class VKTextView extends TextView {
	

	public VKTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public VKTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VKTextView(Context context) {
        super(context);
    }

    public void setTypeface(Typeface tf, int style) {

        Typeface normalTypeface = Typeface.createFromAsset(getContext().getAssets(), "helvetica.ttf");
        Typeface boldTypeface = Typeface.createFromAsset(getContext().getAssets(),  "myriadpro_bold.ttf");
        if (style == Typeface.BOLD) {
            super.setTypeface(boldTypeface/*, -1*/);
        } else {
            super.setTypeface(normalTypeface/*, -1*/);
        }
    }

}
